import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { AppURL } from '../../apiUrl';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class GraphsService {

  constructor(private http: Http, private httpClient: HttpClient) {
  }

  getWeatherData() {
    return this.http.get(AppURL.api + 'wSensors').map(res => res.json());
  }

  getAirPressureData() {
    return this.http.get(AppURL.api + 'ApSensors').map(res => res.json());
  }

  getMapData() {
    return this.http.get(AppURL.api + 'Mapviews').map(res => res.json());
  }
  // getAirData() {
  //   return this.http.get(AppURL.api1 + 'wines/webapi/getProducts/data').map(res => res.json());
  // }
  getStreaming() {
    return this.http.get(AppURL.api2 + 'getStreaming').map(res => res.json());
  }
  getHeat() {
    return this.http.get(AppURL.api2 + 'getHeat').map(res => res.json());
  }
  getPressure() {
    return this.http.get(AppURL.api2 + 'getPressure').map(res => res.json());
  }
  getWind() {
    return this.http.get(AppURL.api2 + 'getWind').map(res => res.json());
  }
  getWaterLevel() {
    return this.http.get(AppURL.api2 + 'getWaterLevel').map(res => res.json());
  }
  getHumidity() {
    return this.http.get(AppURL.api2 + 'getHumidity').map(res => res.json());
  }
  getDeviceInfoall() {
    return this.http.get(AppURL.api1 + 'DeviceInfos').map(res => res.json());
  }
  getDeviceInfo(city) {
    return this.http.get(AppURL.api1 + 'DeviceInfos?filter=%7B%22where%22%3A%7B%22city%22%3A%20%22' + city + '%22%7D%7D').map(res => res.json());
  }
  getIncidentRoad(city) {
    return this.http.get(AppURL.api1 + 'Incidents?filter=%7B%22where%22%20%3A%20%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + city + '%22%7D%2C%20%7B%22type%22%3A%20%22bridge%2Froad%20collapse%22%7D%5D%7D%7D').map(res => res.json());
  }
  getIncidentAccident(city) {
    return this.http.get(AppURL.api1 + 'Incidents?filter=%7B%22where%22%20%3A%20%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + city + '%22%7D%2C%20%7B%22type%22%3A%20%22Accident%20emergency%22%7D%5D%7D%7D').map(res => res.json());
  }
  getIncidentFire(city) {
    return this.http.get(AppURL.api1 + 'Incidents?filter=%7B%22where%22%20%3A%20%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + city + '%22%7D%2C%20%7B%22type%22%3A%20%22Fire%22%7D%5D%7D%7D').map(res => res.json());
  }
  getIncidentNatural(city) {
    return this.http.get(AppURL.api1 + 'Incidents?filter=%7B%22where%22%20%3A%20%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + city + '%22%7D%2C%20%7B%22type%22%3A%20%22Natural%20Disaster%22%7D%5D%7D%7D').map(res => res.json());
  }
  getIncidentMass(city) {
    return this.http.get(AppURL.api1 + 'Incidents?filter=%7B%22where%22%20%3A%20%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + city + '%22%7D%2C%20%7B%22type%22%3A%20%22mass%20crowd%22%7D%5D%7D%7D').map(res => res.json());
  }
  getIncidentParking(city) {
    return this.http.get(AppURL.api1 + 'Incidents?filter=%7B%22where%22%20%3A%20%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + city + '%22%7D%2C%20%7B%22type%22%3A%20%22parking%22%7D%5D%7D%7D').map(res => res.json());
  }
  trueDeskPost(data) {
    return this.http.post(AppURL.api1 + `TrueDesks`, data).map((res: Response) => res);
  }
  waterPoliceService(data) {
    return this.http.post(AppURL.api1 + `Police`, data).map((res: Response) => res);
  }
  waterFireService(data) {
    return this.http.post(AppURL.api1 + `Fires`, data).map((res: Response) => res);
  }
  waterHospitalervice(data) {
    return this.http.post(AppURL.api1 + `Hospitals`, data).map((res: Response) => res);
  }
  tempPoliceService(data) {
    return this.http.post(AppURL.api1 + `Police`, data).map((res: Response) => res);
  }
  // getLatestNews(x, y) {
  //   return this.http.get(AppURL.api1 + 'LatestNews?filter=%7B%22where%22%3A%7B%22and%22%20%3A%20%5B%7B%22city%22%3A%20%22' + x +'%22%7D%2C%20%7B%22town%22%3A%20%22' + y +'%22%7D%5D%7D%7D').map(res => res.json());
  // }
  getAlerts() {
    return this.http.get(AppURL.api1 + 'PushMessages').map(res => res.json());
  }
  updateAlert(x) {
    return this.http.put(AppURL.api1 + 'PushMessages', x).map(res => res.json());
  }
  updateIncident(x) {
    return this.http.put(AppURL.api1 + 'PushMessages/updateIncident', x).map(res => res.json());
  }
  updateIncidentStatus(x) {
    return this.http.put(AppURL.api1 + 'PushMessages/updateIncidentStatus', x).map(res => res.json());
  }
  updateSensor(x) {
    return this.http.put(AppURL.api3 + 'SmokeInfos', x).map(res => res.json());
  }
  postAlert(x) {
    return this.http.post(AppURL.api1 + 'PushMessages', x).map(res => res.json());
  }
  postTwitter(x) {
    return this.http.post(AppURL.api1 + 'PushMessages/postIncident', x).map(res => res.json());
  }
  createIncident(data) {
    return this.http.post(AppURL.api1 + `Incidents`, data).map((res: Response) => res);
  }
  getIncidents(type) {
    return this.http.get(AppURL.api1 + 'PushMessages?filter=%7B%22where%22%20%3A%20%7B%22alertType%22%3A%20%22' + type + '%22%7D%7D').map(res => res.json());
  }
  getTwitterData(hash) {
    return this.http.get(AppURL.api1 + 'twitterHashTags/getTwitterHashTags?input=%23' + hash).map(res => res.json());
  }
  sendSMS(data) {
    return this.http.post(AppURL.api1 + `sms`, data).map((res: Response) => res);
  }

  updateLightStat(data) {
    return this.http.put(AppURL.api1 + 'SmartPoles', data).map(res => res.json());
  }
  getRealSensor() {
    return this.http.get(AppURL.api3 + 'SmokeInfos?filter=%7B%22where%22%3A%7B%22and%22%3A%20%5B%7B%22smoke%22%3A%20%22yes%22%7D%2C%20%7B%22count%22%3A%20%221%22%7D%5D%7D%7D').map(res => res.json());
  }
  getParkingdata() {
    return this.http.get(AppURL.api1 + 'SmartPoles').map((res: Response) => res.json());
  }
  sendMail(data) {
    return this.http.post(AppURL.api1 + `Escalations/sendEmail`, data).map((res: Response) => res);
  }
  createEscalation(data) {
    return this.http.post(AppURL.api1 + `Escalations`, data).map((res: Response) => res);
  }
  getParkingSpace(x) {
    return this.http.get(AppURL.api1 + 'Parkings?filter=%7B%22where%22%3A%20%7B%22cityName%22%3A%20%22' + x + '%22%7D%7D').map((res: Response) => res.json());
  }
  getUrl() {
    return this.http.get(AppURL.api1 + 'CamUrls').map(res => res.json());
  }
  updateUrl(data) {
    return this.http.put(AppURL.api1 + 'CamUrls', data).map(res => res.json());
  }
  getRealTiemeParking() {
    return this.http.get(AppURL.api1 + 'Csvfiles').map(res => res.json());
  }
  getregisterData() {
    return this.http.get(AppURL.api1 + 'Urbansprawls').map(res => res.json());
  }
  // environmental sensor integrations
  loginEnv(data) {
    return this.http.post(AppURL.envLoginApi, data).map((res: Response) => res);
  }

  getEvn(x) {
    // debugger
    //   let head = new Headers({ 'Content-Type': 'application/json','Authorization':x });
    // let options = new RequestOptions({ headers: head });
    // let username: any = x;
    // let password: string = 'xxxx';

    // let token: string = btoa(username);

    // let headers: Headers = new Headers();
    // headers.append('Access-Control-Expose-Headers', 'Authorization');
    // headers.append('Authorization', 'Basic ' + token);
    // headers.append("Access-Control-Allow-Origin", "http://localhost:4200/");
    // headers.append("Access-Control-Allow-Methods", "*");
    // headers.append("Access-Control-Allow-Headers", "Accept,Accept-Charset,Accept-Encoding,Accept-Language,Authorization,Connection,Content-Type,Cookie,DNT,Host,Keep-Alive,Origin,Referer,User-Agent,X-CSRF-Token,X-Requested-With");
    // headers.append("Access-Control-Allow-Credentials", "true");

    // return headers;
    // let options = {} ;
    // options = {
    //   headers: new HttpHeaders().set('Authorization', x)
    // }
    // debugger;
    const httpOptions = { headers: new HttpHeaders() };

    httpOptions.headers = httpOptions.headers.set('Authorization', x);
    return this.httpClient.get(AppURL.envLoginApi1, httpOptions).map(res => res);

  }

  getwasteBinData() {

    //   let head = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: head });
    return this.http.get(AppURL.api1 + 'RealBins').map(res => res.json());
  }
  getRealTimeWifi() {
    return this.http.get(AppURL.wifiAPI).map(res => res.json());
  }

  getWifiData() {
    return this.http.get(AppURL.api1 + 'Realtimewifis').map(res => res.json());
  }
  getPoleData() {
    return this.http.get(AppURL.api1 + 'SmartPoles').map(res => res.json());
  }
  getRealEnvData() {
    return this.http.get(AppURL.api1 + 'Devicedetails').map(res => res.json());
  }
  getBulkcontacts() {
    return this.http.get(AppURL.api1 + 'BulkSMs').map(res => res.json());
  }
  getBulkcontacts1(data) {
    return this.http.get(AppURL.api1 + 'BulkSMs?filter=%7B%20%22where%22%3A%20%7B%22area%22%3A%22' + data + '%22%20%7D%20%7D').map(res => res.json());
  }
  getTrafficData() {
    return this.http.get(AppURL.api1 + 'Websockets/getWebsocket').map(res => res.json());
  }
  getParkingAPI() {
    return this.http.get(AppURL.api1 + 'Devicedetails/dDetails').map(res => res.json());
  }
  getPoleAPI() {
    return this.http.get(AppURL.api1 + 'SmartPoles/SmartPoleData').map(res => res.json());
  }
  getWifiAPI() {
    return this.http.get(AppURL.api1 + 'Realtimewifis/test').map(res => res.json());
  }
  getBinAPI() {
    return this.http.get(AppURL.api1 + 'RealBins/getBinData').map(res => res.json());
  }
  getSound1() {
       return this.http.get(AppURL.sound1).map(res => res.json());
      }
      getSound2(x) {
        return this.http.get(AppURL.sound2 + x).map(res => res.json());
      }
      getRealLight(x) {
        return this.http.get(AppURL.api1 + 'SmartPoles/getRealPole', x).map(res => res.json());
       }
       getSound3(x) {
        return this.http.get(AppURL.api1 + 'BulkSMs/getAnnouncement', x).map(res => res.json());
       }
}
