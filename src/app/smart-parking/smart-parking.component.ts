import { Component, OnInit} from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
// import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
declare var $: any;

@Component({
  selector: 'app-smart-parking',
  templateUrl: './smart-parking.component.html',
  styleUrls: ['./smart-parking.component.css']
})
export class SmartParkingComponent implements OnInit {
 
  airPurityStatus: string;
  latParking: number = 11.3272889;
  lngParking: number = 78.07734;
  zoomControl: boolean = true;
  zoom1: any = 8;
  options: any;
  severity1: boolean;
  deficientVar: boolean;
  excessVar: boolean;
  moderateVar: boolean;
  goodVar: boolean;
  severeVar: boolean;
  noiseVar: boolean;
  oVar: boolean;
  noVar: boolean;
  soVar: boolean;
  coVar: boolean;
  severity: any;
  type: any;
  city: any;
  area: any;
  selectedCityName: any;
  smartCityInfo: any;
  todayDate: any;
  clickedMarker2(label: any, index: number) {
    console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
    this.area = label.label;
    this.city = label.city;
    this.type = label.type;
    this.severity = label.severity;
   
  }
  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event);
  }
  mouseOver2($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
    // debugger;
    this.area = label;
    this.city = city;
    this.type = type;
    this.severity = severity;
    if (this.type == 'Excess Suply') {
      this.excessVar = true;
      this.deficientVar = false;
    } else if (this.type == 'Deficient') {
      this.deficientVar = true;
      this.excessVar = false;
    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  markerDragEnd(m: marker2, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  ngOnInit() {
    this.markers2 = [

      {
        lat: 11.12254185885016,
        lng: 76.92789541992192,
        label: '1,880',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Coimbatore',
        severity: '46,54,206',
        type: 'parking',
      },
      {
        lat: 9.929476032244802,
        lng: 78.10891906330528,
         label: '2,620',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Madurai',
        severity: '43,93,956',
        type: 'parking',
      },
      {
        lat: 11.659632671616086,
        lng: 78.14169532816027,
         label: '1,310',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Salem',
        severity: '35,45,139',
        type: 'parking',
      },
      {
        lat: 10.787,
        lng: 79.1378,
        label: '2,620',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Thanjavur',
        severity: '45,61,463',
        type: 'parking',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        label: '1,310',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Tiruchirapalli',
        severity: '33,45,139',
        type: 'parking',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        label: '1,310',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Tiruppur',
        severity: '33,55,139',
        type: 'parking',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        label: '1,310',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Vellore',
        severity: '33,55,139',
        type: 'parking',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        label: '1,310',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Tirunelveli',
        severity: '33,57,139',
        type: 'parking',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        label: '1,210',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Thoothkudi',
        severity: '33,67,139',
        type: 'parking',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        label: '1,310',
        draggable: true,
        iconUrl: '../assets/images/parking_3.png',
        city: 'Erode',
        severity: '33,57,139',
        type: 'parking',
      },
    ]
    this.todayDate = new Date();
    // this.onSelect({"name":"Coimbatore","value":30});
    this.coVar = false;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity = true;
    this.airPurityStatus = 'pm';
    // this.severity = false;
    this.severity1 = true;
    this.options = {
      title: { text: '' },
      xAxis: {
        categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
        max:6
    },
    yAxis: {
      title: { text: 'INR' }
    },
    tooltip:{
      borderWidth: 0,
      shadow: false,
      style: {
        padding: 0
      },
              formatter:function(){
              return 'INR '+ this.y;
              }
          },
			series: [{
				name: 'Time Line',
				data: [2122579, 2410257,  2388414, 2256087, 1958543, 2366365, 1985927],
				tooltip: {
					valueDecimals: 2
				},
				allowPointSelect: true
			}]
		};
  }
  weekly() {
    this.options = {
      title: { text: '' },
      xAxis: {
        categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
        max:6
    },
    yAxis: {
      title: { text: 'INR' }
    },
    tooltip:{
      borderWidth: 0,
      shadow: false,
      style: {
        padding: 0
      },
              formatter:function(){
              return 'INR '+ this.y;
              }
          },
			series: [{
				name: 'Time Line',
				data: [2122579, 2410257,  2388414, 2256087, 1958543, 2366365, 1985927],
				tooltip: {
					valueDecimals: 2
				},
				allowPointSelect: true
			}]
		};
  }
  monthly() {
    this.options = {
      title: { text: '' },
      xAxis: {
        categories: ['1-3', '4-6', '7-9', '10-12','13-15','16-18','19-21','22-24','25-27','28-30'],
        max:11
    },
    yAxis: {
      title: { text: 'INR' }
    },
    tooltip:{
      borderWidth: 0,
      shadow: false,
      style: {
        padding: 0
      },
              formatter:function(){
              return 'INR '+ this.y;
              }
          },
			series: [{
        name: 'Time Line',
				data: [2723755, 3092598,  3247914, 2634908, 2875269, 2712157, 2969843, 3123782, 3084343, 3293756],
				tooltip: {
					valueDecimals: 2
				},
				allowPointSelect: true
			}]
		};
  }
  yearly() {
    this.options = {
      title: { text: '' },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
        max:11
    },
    yAxis: {
      title: { text: 'INR' }
    },
    tooltip:{
      borderWidth: 0,
      shadow: false,
      style: {
        padding: 0
      },
              formatter:function(){
              return 'INR '+ this.y;
              }
          },
			series: [{
				name: 'Time Line',
				data: [4791781, 3637392,  3691154, 4129662, 4348788, 4462913, 3617285, 4084549, 4946628, 4475782,4475787,4475797],
				tooltip: {
					valueDecimals: 2
				},
				allowPointSelect: true
			}]
		};
  }
  markers2: marker2[] = [

    {
      lat: 11.12254185885016,
      lng: 76.92789541992192,
      label: '1,880',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Coimbatore',
      severity: '46,54,206',
      type: 'parking',
    },
    {
      lat: 9.929476032244802,
      lng: 78.10891906330528,
       label: '2,620',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Madurai',
      severity: '43,93,956',
      type: 'parking',
    },
    {
      lat: 11.659632671616086,
      lng: 78.14169532816027,
       label: '1,310',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Salem',
      severity: '35,45,139',
      type: 'parking',
    },
    {
      lat: 10.787,
      lng: 79.1378,
      label: '2,620',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Thanjavur',
      severity: '45,61,463',
      type: 'parking',
    },
    {
      lat: 10.7905,
      lng: 78.7047,
      label: '1,310',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Tiruchirapalli',
      severity: '33,45,139',
      type: 'parking',
    },
    {
      lat: 11.1085,
      lng: 77.3411,
      label: '1,310',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Tiruppur',
      severity: '33,55,139',
      type: 'parking',
    },
    {
      lat: 12.9165,
      lng: 79.1325,
      label: '1,310',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Vellore',
      severity: '33,55,139',
      type: 'parking',
    },
    {
      lat: 8.7139,
      lng: 77.7567,
      label: '1,310',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Tirunelveli',
      severity: '33,57,139',
      type: 'parking',
    },
    {
      lat: 8.7642,
      lng: 78.1348,
      label: '1,210',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Thoothkudi',
      severity: '33,67,139',
      type: 'parking',
    },
    {
      lat: 11.3410,
      lng: 77.7172,
      label: '1,310',
      draggable: true,
      iconUrl: '../assets/images/parking_3.png',
      city: 'Erode',
      severity: '33,57,139',
      type: 'parking',
    },
  ]
  constructor(private serviceAssert:GetDigitalAssetType, private router: Router) {
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
  }
  

}
interface marker2 {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}
$(window).scroll(function () {
	var scroll = $(window).scrollTop();

	//>=, not <=
	if (scroll >= 100) {
		//clearHeader, not clearheader - caps H
		$("#incidentMapArea").addClass("darkHeader");
	}
	if (scroll <= 10) {
		//clearHeader, not clearheader - caps H
		$("#incidentMapArea").removeClass("darkHeader");
  }
})