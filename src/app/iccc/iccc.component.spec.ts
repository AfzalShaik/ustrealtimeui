import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IcccComponent } from './iccc.component';

describe('IcccComponent', () => {
  let component: IcccComponent;
  let fixture: ComponentFixture<IcccComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IcccComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IcccComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
