import { Component, OnInit} from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
declare var $: any;

@Component({
  selector: 'app-state-all',
  templateUrl: './state-all.component.html',
  styleUrls: ['./state-all.component.css']
})
export class StateAllComponent implements OnInit {
  private chart: AmChart;
  constructor(private serviceAssert:GetDigitalAssetType, private router: Router,private AmCharts: AmChartsService) { 
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
  }

  ngOnInit() {
    this.weekly13d();
    this.weekly23d();
    this.weekly33d();
    // this.weekly43d();
    this.chart = this.AmCharts.makeChart("chartdiv0", {
      "theme": "light",
      "type": "serial",
      "hideCredits":true,
    "startDuration": 2,
      "dataProvider": [{
          "country": "Coimbatore",
          "visits": 80,
          "color": "#FF6600"
      }, {
          "country": "Madurai",
          "visits": 35,
          "color": "#FF6600"
      },
      {
        "country":"Salem",
        "visits": 14,
        "color": "#FF6600"
    },
    {
      "country": "Thanjavur",
      "visits": 67,
      "color": "#FF6600"
  },
  {
    "country": "Tiruchirapalli",
    "visits": 42,
    "color": "#FF6600"
},
{
  "country": "Vellore",
  "visits": 23,
  "color": "#FF6600"
},
{
  "country": "Erode",
  "visits": 100,
  "color": "#FF6600"
},
{
  "country": "Tirunelveli",
  "visits": 76,
  "color": "#FF6600"
},
{
  "country": "Tiruppur",
  "visits": 87,
  "color": "#FF6600"
},
{
  "country": "Thoothukudi",
  "visits": 76,
  "color": "#FF6600"
},
    ],
      "titles": [{
        "text": "INCIDENTS HAPPENED IN CITIES"
      }],
      "graphs": [{
          "balloonText": "[[category]]: <b>[[value]]</b>",
          "fillColorsField": "color",
          "fillAlphas": 1,
          "lineAlpha": 0.1,
          "type": "column",
          "valueField": "visits"
      }],
      "depth3D": 20,
    "angle": 30,
      "chartCursor": {
          "categoryBalloonEnabled": false,
          "cursorAlpha": 0,
          "zoomable": false
      },
      "categoryField": "country",
      "categoryAxis": {
          "gridPosition": "start",
          "labelRotation":60
      },
      "export": {
        "enabled": true
       }
  
  });
    
    this.chart = this.AmCharts.makeChart("chartdiv1", {
      "theme": "light",
      "type": "serial",
      "hideCredits":true,
    "startDuration": 2,
      "dataProvider": [{
          "country": "Emergency",
          "visits": 106,
          "color": "#FF0F00"
      }, {
          "country": "Non Emergency",
          "visits": 1488,
          "color": "#FF6600"
      }],
      "titles": [{
        "text": "Incidents By Priorty-Current Week"
      }],
      "graphs": [{
          "balloonText": "[[category]]: <b>[[value]]</b>",
          "fillColorsField": "color",
          "fillAlphas": 1,
          "lineAlpha": 0.1,
          "type": "column",
          "valueField": "visits"
      }],
      "depth3D": 20,
    "angle": 30,
      "chartCursor": {
          "categoryBalloonEnabled": false,
          "cursorAlpha": 0,
          "zoomable": false
      },
      "categoryField": "country",
      "categoryAxis": {
          "gridPosition": "start",
          "labelRotation": 0
      },
      "export": {
        "enabled": true
       }
  
  });
  this.chart = this.AmCharts.makeChart( "chartdiv", {
    "type": "pie",
    "hideCredits":true,
    "theme": "light",
    "dataProvider": [ {
      "country": "Fire",
      "value": 10
    }, {
      "country": "Road accidents",
      "value": 55
    }, 
    {
      "country": "Riots",
      "value": 2
    },
    {
      "country": "Stampede",
      "value": 1
    }, 
    {
      "country": "Building Collapse",
      "value": 6
    },
    {
      "country": "Crime",
      "value": 32
    }],
    "titles": [{
      "text": "Incidents By Category-Current Week"
    }],
    "valueField": "value",
    "titleField": "country",
    "outlineAlpha": 0.4,
    "depth3D": 15,
    // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "angle": 30,
    "export": {
      "enabled": true
    }
  });
  }
  weekly13d(){
    this.chart = this.AmCharts.makeChart( "chartdiv2", {
    "type": "serial",
    "theme": "light",
    "hideCredits":true,
    "legend": {
        "horizontalGap": 10,
        "maxColumns": 1,
        "position": "right",
		"useGraphSettings": true,
		"markerSize": 10
    },
    "titles": [{
      "text": "Current Month"
    }],
    "dataProvider": [{
        "year": "week1",
        "europe": 10,
        "namerica": 55,
        "asia": 2,
        "lamerica": 1,
        "meast": 6,
        "africa": 32
    }, {
      "year": "week2",
      "europe": 6,
      "namerica":32,
      "asia": 3.5,
      "lamerica":2,
      "meast": 8,
      "africa": 35
    }, {
      "year": "week3",
      "europe": 14,
      "namerica": 59,
      "asia": 5,
      "lamerica":4,
      "meast": 9,
      "africa":40
    },
    {
      "year": "week4",
      "europe": 3,
      "namerica":28,
      "asia": 5,
      "lamerica":4,
      "meast": 10,
      "africa": 43
    }
  ],
    "valueAxes": [{
        "stackType": "regular",
        "axisAlpha": 0.3,
        "gridAlpha": 0
    }],
    "graphs": [{
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Fire",
        "type": "column",
		"color": "#000000",
        "valueField": "europe"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Road Accidents",
        "type": "column",
		"color": "#000000",
        "valueField": "namerica"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Riots",
        "type": "column",
		"color": "#000000",
        "valueField": "asia"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Stampede",
        "type": "column",
		"color": "#000000",
        "valueField": "lamerica"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Building-collapse",
        "type": "column",
		"color": "#000000",
        "valueField": "meast"
    }, {
        "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        "fillAlphas": 0.8,
        "labelText": "[[value]]",
        "lineAlpha": 0.3,
        "title": "Crime",
        "type": "column",
		"color": "#000000",
        "valueField": "africa"
    }],
    "categoryField": "year",
    "categoryAxis": {
        "gridPosition": "start",
        "axisAlpha": 0,
        "gridAlpha": 0,
        "position": "left"
    },
    "export": {
    	"enabled": true
     }

});
  }
  weekly23d(){
    this.chart = this.AmCharts.makeChart("chartdiv3", {
    "theme": "light",
    "type": "serial",
    "hideCredits":true,
    "titles": [{
      "text": "By Escalate"
    }],
    "dataProvider": [{
      "country": "Fire",
      "escalate": 35.5,
      "non-escalte": 65.5
  }, {
      "country": "Road Accidents",
      "escalate": 40.7,
      "non-escalte": 59.3
  }, {
      "country": "Riots",
      "escalate": 27.8,
      "non-escalte": 72.2
  }, {
      "country": "Stampede",
      "escalate": 26.6,
      "non-escalte": 73.4
  }, {
      "country": "Building-Collapse",
      "escalate": 14.4,
      "non-escalte": 85.6
  }, {
      "country": "Crime",
      "escalate": 26.6,
      "non-escalte": 73.4
  }],
  "valueAxes": [{
      "stackType": "3d",
      "unit": "%",
      "position": "left",
      // "title": "GDP growth rate",
  }],
  "startDuration": 1,
  "graphs": [{
      "balloonText": "[[category]] (escalate): <b>[[value]]</b>",
      "fillAlphas": 0.9,
      "lineAlpha": 0.2,
      "title": "escalate",
      "type": "column",
      "valueField": "escalate"
  }, {
      "balloonText": "[[category]] (non-escalate): <b>[[value]]</b>",
      "fillAlphas": 0.9,
      "lineAlpha": 0.2,
      "title": "non-escalte",
      "type": "column",
      "valueField": "non-escalte"
  }],
    "plotAreaFillAlphas": 0.1,
    "depth3D": 30,
    "angle": 30,
    "categoryField": "country",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation":40

    },
    "export": {
    	"enabled": true
     }
});
  }
  weekly33d(){
    this.chart = this.AmCharts.makeChart( "chartdiv4",  {
    "type": "pie",
    "hideCredits":true,
    "theme": "light",
    "dataProvider": [ {
      "country": "OPEN",
      "value": 37,
    }, {
      "country": "CLOSED",
      "value": 63,
    }],
    "titles": [{
      "text": "Incidents Open and Closed"
    }],
    "valueField": "value",
    "titleField": "country",
    "outlineAlpha": 0.4,
    "depth3D": 15,
    // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
    "angle": 30,
    "export": {
      "enabled": true
    }
  });
  }
}
