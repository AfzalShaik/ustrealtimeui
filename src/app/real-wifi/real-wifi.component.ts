import { Component, OnInit, NgModule } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { Time } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GetDigitalAssetType } from '../digital-asset-dashboard/digital-asset-dashboard.services';
import { toUnicode } from 'punycode';
declare var $: any;

@Component({
  selector: 'app-real-wifi',
  templateUrl: './real-wifi.component.html',
  styleUrls: ['./real-wifi.component.css'],
  providers: [GraphsService, GetDigitalAssetType]
})
export class RealWifiComponent implements OnInit {
  zoom1: number = 12;
  lat: any = 13.0480438;
  lng: any = 80.1389196;
  paramData: any;
  city: any;
  info1: any;
  goodVar: boolean;
  bandwidth: any;
  host: any;
  info: any;
  location: any;
  constructor(private GS: GraphsService, private route: ActivatedRoute, private serviceAssert: GetDigitalAssetType) { }

  ngOnInit() {
    this.getwifiData();
    this.paramData = this.route.params.subscribe(params => {
      this.city = params.id;
      this.info1 = this.city;
    });
  }
  getwifiData() {
    this.GS.getWifiAPI().subscribe(data => {
      let obj = {};
      obj = data.data.Items[0];
      this.wifimarkers = [
        {
          lat:  13.075724500799785,
          lng:  80.18034481996983,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/wifi_on.png',
          info: obj['payload'].M.info.S,
          type: '',
          bandwidth: obj['payload'].M.bandwidth.S,
          hostname: obj['payload'].M.Hostname.S,
          location: obj['payload'].M.location.S,
        }
      ]

    });
  }
  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event.coords);
    
  }
  clickedMarker(label: any,infoWindow, gm) {
    this.goodVar = true;
    this.bandwidth = label.bandwidth;
    this.host = label.hostname;
    this.info = label.info;
    this.location = label.location;
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
   
  }
  mouseOver($event: MouseEvent, label: any, infoWindow, gm) {
    this.goodVar = true;
    this.bandwidth = label.bandwidth;
    this.host = label.hostname;
    this.info = label.info;
    this.location = label.location;
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  wifimarkers: wifiMarker[] = [
    {
      lat:  13.075724500799785,
      lng:  80.18034481996983,
      label: '',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      bandwidth: '',
      hostname: '',
      location: '',
    }
  ]
}
interface wifiMarker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: any;
  type: string;
  bandwidth: any;
  hostname: any;
  location: any;
}