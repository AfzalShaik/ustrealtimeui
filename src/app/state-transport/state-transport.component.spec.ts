import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StateTransportComponent } from './state-transport.component';

describe('StateTransportComponent', () => {
  let component: StateTransportComponent;
  let fixture: ComponentFixture<StateTransportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StateTransportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StateTransportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
