import { Component, OnInit, NgModule } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { Time } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GetDigitalAssetType } from '../digital-asset-dashboard/digital-asset-dashboard.services';
import { toUnicode } from 'punycode';
declare var $: any;
import { ChartModule } from 'angular2-highcharts';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'app-state-transport',
  templateUrl: './state-transport.component.html',
  styleUrls: ['./state-transport.component.css'],
  providers: [GraphsService, GetDigitalAssetType]
})
export class StateTransportComponent implements OnInit {
  private chart: AmChart;
  transportZoom: any;
  wasteZoom: number;
  zoomWater: number;
  clickedVar: boolean;
  severeVar: boolean;
  goodVar: boolean;
  moderateVar: boolean;
  discount: any;
  fee: any;
  typeOfParking: any;
  city: any;
  paramData: any;
  single2: { "name": string; "value": number; }[];
  label: any;
  timeVar: any;
  cityArray: any;
  deviceArray: any;
  info1: any;
  type: any;
  previousReading: any;
  currentReading: any;
  time: any;
  bill: any;
  status: any;
  single: any[];
  single1: any[];
  lights: any[];
  wifiavil: any[];
  browsing: any[];
  citys: any[];
  wifibandwidth: any[];
  view: any[] = [750, 400];
  view11: any[] = [600, 500];
  wifiview: any[] = [200, 300];
  wifiusing: any[] = [250, 300];
  // options
  viewforWaterDistrib: any[] = [200, 300];
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  poleData: any;
  updatedLightData: any;
  lightOnURL: any;
  noParking: any;
  parkingVar: any;
  waterViewDetails = [
    {
      "name": "Total Flow",
      "value": 40000
    },
    {
      "name": "Distributed",
      "value": 30000
    }
  ]
  waterBillMonthWise: any = [
    {
      "name": "Coimbatore",
      "value": 4000
    }
  ];
  ////////////////pie chate///////////////
  single4: any = [{
    "name": "Availability",
    "value": 1500
  },
  {
    "name": "Full",
    "value": 380
  }];
  multi: any[];

  view1: any[] = [700, 400];

  // options
  showLegend1 = false;



  // pie
  showLabels1 = true;
  explodeSlices = false;
  doughnut = false;

  ////////////////end pie chate///////////////

  constructor(private GS: GraphsService, private route: ActivatedRoute, private serviceAssert: GetDigitalAssetType, private AmCharts: AmChartsService) {
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
    });
    
    $(document).ready(function () {
      //Toggle fullscreen
      $("#panel-fullscreen").click(function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.children('i').hasClass('glyphicon-resize-full')) {
          $this.children('i').removeClass('glyphicon-resize-full');
          $this.children('i').addClass('glyphicon-resize-small');
          $('.holder').css({
            'min-height': '600px'
          });
          $('#tabsArea .panel-default').css({
            'padding': '20px',
            'margin-top': '95px'
          });
        }
        else if ($this.children('i').hasClass('glyphicon-resize-small')) {
          $this.children('i').removeClass('glyphicon-resize-small');
          $this.children('i').addClass('glyphicon-resize-full');
          $('.holder').css({
            'min-height': '250px'
          });
          $('#tabsArea .panel-default').css({
            'padding': '0px',
            'margin-top': '0px'
          });
        }
        $(this).closest('.panel').toggleClass('panel-fullscreen1');

      });
    })
    this.single = [
      {
        "name": "Working",
        "value": 20
      },
      {
        "name": "Stopped",
        "value": 32
      },
      {
        "name": "Maintanance",
        "value": 14
      },
      {
        "name": "Deployed",
        "value": 20
      }
    ];
    this.single1 = [
      {
        "name": "Working",
        "value": 11
      },
      {
        "name": "Stopped",
        "value": 8
      },
      {
        "name": "Maintanance",
        "value": 18
      },
      {
        "name": "Deployed",
        "value": 15
      }
    ];
    this.single2 = [
      {
        "name": "Working",
        "value": 26
      },
      {
        "name": "Stopped",
        "value": 12
      },
      {
        "name": "Maintanance",
        "value": 14
      },
      {
        "name": "Deployed",
        "value": 35
      }
    ];


  }
  zoomlevel: number;
  checkZoomLevel(city): number {

    switch (city) {
      case 'Coimbatore':
        this.zoomlevel = 14;
        break;
      case 'Madurai':
        this.zoomlevel = 13;
        break;
      case 'Salem':
        this.zoomlevel = 16;
        break;
      case 'Thanjavur':
        this.zoomlevel = 17;
        break;

      case 'Tiruchirapalli':
        this.zoomlevel = 17;
        break;
      case 'Vellore':
        this.zoomlevel = 18;
        break;
      case 'Tirunelveli':
        this.zoomlevel = 17;
        break;
      case 'Tiruppur':
        this.zoomlevel = 17;
        break;
      case 'Thoothukudi':
        this.zoomlevel = 17;
        break;
      case 'Erode':
        this.zoomlevel = 17;
        break;
      default:
        this.zoomlevel = 17;
    }

    return this.zoomlevel;
  }

  ngOnInit() {
    this.allcities();
    this.smartTransport('Coimbatore');
    this.timeVar = new Date();
    this.timeVar = this.timeVar;
    this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode'];
    this.GS.getDeviceInfo('Coimbatore').subscribe(data => {
      console.log('device infoooooo ', JSON.stringify(data));
      this.deviceArray = data[0];
    });
    this.paramData = this.route.params.subscribe(params => {
      this.city = params.id;
      this.info1 = this.city;
      // this.setCollectionBills();
      $('#assertdashboard a[href="#tab1"]').trigger('click');
      this.pressureChanged(this.city);
    });
   
    this.lightOnURL = '../assets/images/lightOn.png'

    
  }
/**
 * To set the bill collection for current city
 * info1 - current city name
 * waterBillMonthWise- bar chart data
 * @memberof DigitalAssetDashboardComponent
 */


  zoom: any = 8.5;
  // iconUrl:'../assets/images/green.png';
  iconUrl: string = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';
  // initial center position for the map
  lat: number = 11.1271;
  lng: number = 78.6569;
  parkingAvil: number;
  bandwidth: string;
  bandwidthkey: string;
  typeOfData: any = { "first": 'Previous Reading', "secound": 'Current Reading' }
  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event.coords);
    
  }
  clickedMarker(label: any, index: number, information: string, type: string, previousReading: any, currentReading: any, time: any, status: string, bill: any, bandwidth: any, lightStatus:string, typeOfParking: any,fee:any,discount:any, infoWindow, gm) {
    // console.log(`clicked the marker: ${label || index}`);
    // console.log('clicked the marker:', JSON.stringify(information));
    // console.log('typeeeeeeeeeeee ', label);

    // this.zoom  ++ ;
    this.bandwidth = '';
    this.info1 = label.cityName;
    this.type = label.info;
    this.previousReading = label.previousReading;
    this.currentReading = label.currentReading;
    this.time = time;
    this.status = label.status;
    this.bill = bill;
    this.typeOfParking = typeOfParking;
    this.fee = fee;
    this.discount = discount;
    this.clickedVar = true;
    if (status == 'About To Fill') {
      this.moderateVar = true;
      this.goodVar = false;
      this.severeVar = false;
    }
    if (status == 'Available') {
      this.moderateVar = false;
      this.goodVar = true;
      this.severeVar = false;
    }
    if (status == 'Not Available') {
      this.moderateVar = false;
      this.goodVar = false;
      this.severeVar = true;
    }
    if (type == 'Parking Availability' || type == 'Waste Management')
      this.single4 = [{ name: 'Available', value: previousReading }, { name: 'Full', value: currentReading }]

    if (type == 'Street Light') {
      this.lights = [
        {
          "name": "Working",
          "value": previousReading
        },
        {
          "name": "Stoped",
          "value": currentReading
        }
      ];

    }

    if (type == 'Water Distributed') {

      this.waterViewDetails = [
        {
          "name": "In flow ",
          "value": previousReading
        },
        {
          "name": "Out flow",
          "value": currentReading
        }
      ]


    }
    if (type == 'Wi-Fi') {
      this.wifiavil = [{
        "name": "Availability",
        "value": previousReading
      },
      {
        "name": "Connected",
        "value": currentReading
      }];
      this.bandwidthkey = "bandwidth";
      this.bandwidth = bandwidth;
    }
    if (type == 'Smart Transport') {
      this.currentReadingData = currentReading;
    }

    console.log(type)

    switch (type) {
      case 'Parking Availability':
        this.typeOfData = { "first": 'Availability', "secound": 'Full' }
        break;
      case 'Waste Management':
        this.typeOfData = { "first": 'Availability', "secound": 'Full' }
        break;
      case 'Street Light':
        this.typeOfData = { "first": 'Working', "secound": 'Stoped' }
        break;
      case 'Wi-Fi':
        this.typeOfData = { "first": 'Availability', "secound": 'Connected' }
        break;

      case 'Water Distributed':
        this.typeOfData = { "first": 'Total Flow', "secound": 'Distribution' }
        break;
      default:
        this.typeOfData = { "first": 'Previous Reading', "secound": 'Current Reading' }
    }
    // debugger;
    // if (gm.lastOpen != null) {
    //   gm.lastOpen.close();
    // }

    // gm.lastOpen = infoWindow;

    // infoWindow.open();

  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }
  

  
  colorScheme = {
    domain: ['#5ca026', '#ed1c24', '#b87b06', '#AAAAAA']
  };
  allcities(){
    this.chart = this.AmCharts.makeChart("chartdiv", {
      "type": "serial",
      "theme": "light",
        "legend": {
            "horizontalGap": 10,
            "maxColumns": 1,
            "position": "right",
        "useGraphSettings": true,
        "markerSize": 10
        },
      //   "graphs": [{
      //     "fillColors": "#008800",
      //     "fillAlpha": 1, //must also be non-zero value; valid values are decimal values <= 1
      //      // ...
      // },
      // repeat for each graph
      // ],
        "dataProvider": [
          {
            "year": "Coimbatore",
            "europe": 600,
            "namerica": 100,
            "asia": 200,
            "lamerica":70,
            // "meast": 900,
        }, {
            "year": "	Madurai",
            "europe": 380,
            "namerica": 95,
            "asia": 125,
            "lamerica":55,
            // "meast": 600,
        }, {
            "year": "Salem",
            "europe": 550,
            "namerica": 75,
            "asia": 125,
            "lamerica":60,
            // "meast": 750,
        }, {
            "year": "Thanjavur",
            "europe": 380,
            "namerica": 75,
            "asia": 145,
            "lamerica":35,
            // "meast": 600,
      }, {
        "year": "Tiruchirapalli",
            "europe": 455,
            "namerica":225,
            "asia": 220,
            "lamerica":70,
            // "meast": 900,
    }, 
    {
      "year": "Vellore",
      "europe": 130,
      "namerica":55,
      "asia": 45,
      "lamerica": 30,
      // "meast": 230,
  }, {
    "year": "Tirunelveli",
    "europe": 576,
    "namerica":100,
    "asia": 100,
    "lamerica":81,
    // "meast": 876,
},
{
  "year": "Tiruppur",
  "europe": 365,
  "namerica":176,
  "asia": 225,
  "lamerica":81,
  // "meast": 766,
}
, {
  "year": "Thoothukudi",
  "europe": 228,
    "namerica":145,
    "asia": 71,
    "lamerica":0,
    // "meast": 444,
}, 
{
  "year": "Erode",
  "europe":432,
  "namerica":110,
  "asia": 258,
  "lamerica": 160,
  // "meast": 800,
}
      ],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.5,
            "gridAlpha": 0
        }],
        "titles": [{
          "text": "ALL CITIES"
        }],
        "graphs": [
        //   {
        //     "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
        //     "fillAlphas": 0.8,
        //     "labelText": "[[value]]",
        //     "lineAlpha": 0.3,
        //     "title": "Total",
        //     "type": "column",
        //     "color": "#000000",
        //     "fillColors":"#aaaaaa",
        //     "valueField": "meast"
        // },
          {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Running",
            "type": "column",
            "color": "#000000",
            "fillColors": "#5ca026",
            "valueField": "europe"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "abandoned",
            "type": "column",
        "color": "#000000",
        "fillColors":"#e60000",
            "valueField": "namerica"
        }, {
            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
            "fillAlphas": 0.8,
            "labelText": "[[value]]",
            "lineAlpha": 0.3,
            "title": "Maintanance",
            "type": "column",
            "color": "#000000",
            "fillColors":"#b87b06",
            "valueField": "asia"
        }],
        "rotate": true,
        "categoryField": "year",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "export": {
          "enabled": true
         }
    })
    }

  showIOTDevice(): void {
    this.zoom = 14;
    this.pressureChanged('Coimbatore')

  };
  //statustrue=false;
  smartPakingAvi: boolean = false;

  smartParking(city): void {
    // debugger;
    this.noParking = false;
    this.parkingVar = true;
    this.viewBusTransport = false;
    //this.statustrue=true;
    this.info1 = this.city;
    this.smartPakingAvi = true;
    this.showConnection = false;
    this.showConnection = false;
    this.zoom = this.checkZoomLevel(this.city);
    this.lat = 11.0157749;
    this.lng = 76.9555643;
    //this.label = 'C';
    this.serviceAssert.getParking(city).subscribe(data => {
      console.log("parking data" + JSON.stringify(data));
      // debugger;
      if (data.length > 0) {
        this.lat = data[0].lat;
        this.lng = data[0].lng;
        //debugger;
        this.noParking = false;
        this.parkingVar = true;
        this.parkingMarkers = data;
       
      }
    }, error => {
      console.log(error)
     

    });
   
    this.single4 = [{
      "name": "Availability",
      "value": 1500
    },
    {
      "name": "Full",
      "value": 380
    }];



    // this.pressureChanged('Coimbatore');

  };
 
  waterview: any[] = [700, 400];
  waterFlow: any = [{
    "name": "Total Flow",
    "value": 56931
  },
  {
    "name": "Distributed",
    "value": 29223
  }]
 
  viewBusTransport = false;
  currentReadingData = [
    { busNum: 'TM 20 K 1209', routeNO: '1A', estimatedTime: '10:02:32' },
    { busNum: 'TM 20 K 4532', routeNO: '9A', estimatedTime: '3:02:32' },
    { busNum: 'TM 20 K 9494', routeNO: '3A', estimatedTime: '04:02:32' }
  ]

  busData = [
    {
      "name": "Total",
      "value": 1248
    },
    {
      "name": "Schedule",
      "value": 892
    },
    {
      "name": "Deviated",
      "value": 314
    },
    {
      "name": "Maintenance",
      "value": 42
    }
  ]
  smartTransport(city: string): void {

    this.viewBusTransport = true;
    this.noParking = true;
    this.parkingVar = false;
    this.single4 = [{
      "name": "Full",
      "value": 200
    },
    {
      "name": "Availability",
      "value": 100
    }];
    this.info1 = this.city;
    this.smartPakingAvi = true;
    this.showConnection = false;
    this.showConnection = false;
    this.zoom = this.checkZoomLevel(this.city);
    // this.lat = 11.019693;
    // this.lng =  76.965171;
    // this.label = 'C';
    // this.serviceAssert.getSmartTrasp(city).subscribe(data => {
    //   console.log("data" + JSON.stringify(data));

    //   if (data.length > 0) {
    //     this.lat = data[0].lat;
    //     this.lng = data[0].lng;
    //     this.markers = data;
    //   }
    // }, error => {
    //   console.log(error)
    // });
    this.transportZoom = 8;
    this.lat = 11.1271;
  this.lng = 78.6569;
    this.transportMarkers = [
      {
        "id": "5afa7ea9342095359f161418",
        "lat": 11.0168,
        "lng": 76.9558,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "100",
        "type": "Smart Transport",
        "previousReading": 900,
        "cityName": "Coimbatore",
        "currentReading": 600,
        "status": "200"
      },
      {
        "id": "5afa7ea9342095359f161419",
        "lat": 9.9252,
        "lng": 78.1198,
        "label": "",
        "draggable": false,
        "cityName": "Madurai",
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "95",
        "type": "Smart Transport",
        "previousReading": 600,
        "currentReading": 380,
        "status": "125"
      },
      {
        "id": "5afd5df9c0c1525898a53f30",
        "lat": 11.664325,
        "lng": 78.146011,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "75",
        "type": "Smart Transport",
        "previousReading": 750,
        "currentReading": 550,
        "status": "125",
        "cityName": "Salem"
      },
      {
        "id": "5afd5df9c0c1525898a53f31",
        "lat": 10.7870,
        "lng": 79.1378,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "75",
        "type": "Smart Transport",
        "previousReading": 600,
        "currentReading": 380,
        "status": "145",
        "cityName": "Thanjavur"
      },
      {
        "id": "5afd5df9c0c1525898a53f32",
        "lat": 10.7905,
        "lng": 78.7047,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "225",
        "type": "Smart Transport",
        "previousReading": 900,
        "currentReading": 455,
        "status": "220",
        "cityName": "Tiruchirapalli"
      },
      {
        "id": "5afd5df9c0c1525898a53f33",
        "lat": 12.9165,
        "lng": 79.1325,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "55",
        "type": "Smart Transport",
        "previousReading": 230,
        "currentReading": 130,
        "status": "45",
        "cityName": "Vellore"
      },
      {
        "id": "5afd5df9c0c1525898a53f34",
        "lat": 8.7139,
        "lng": 77.7567,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "100",
        "type": "Smart Transport",
        "previousReading": 876,
        "currentReading": 576,
        "status": "100",
        "cityName": "Tirunelveli"
      },
      {
        "id": "5afd5df9c0c1525898a53f35",
        "lat": 11.1085,
        "lng": 77.3411,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "176",
        "type": "Smart Transport",
        "previousReading": 766,
        "currentReading": 365,
        "status": "225",
        "cityName": "Tiruppur"
      },
      {
        "id": "5afd5df9c0c1525898a53f35",
        "lat": 8.7642,
        "lng": 78.1348,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "145",
        "type": "Smart Transport",
        "previousReading": 444,
        "currentReading": 228,
        "status": "71",
        "cityName": "Thoothukudi"
      },
      {
        "id": "5afd5df9c0c1525898a53f35",
        "lat": 11.3410,
        "lng": 77.7172,
        "label": "",
        "draggable": false,
        "iconUrl": "http://139.162.45.69:4646/api/Uploads/yitsolDB/download/5afe647405366860caea3256",
        "info": "110",
        "type": "Smart Transport",
        "previousReading": 800,
        "currentReading": 432,
        "status": "258",
        "cityName": "Erode"
      }
    ]
  }
  
 
  showConnection: boolean = false;
  

  pressureChanged(x) {
    // alert('gggggg '+ x);
    this.noParking = true;
    this.parkingVar = false;
    if (x === 'Coimbatore') {
      this.zoom = 15;
      this.lat = 11.0168;
      this.lng = 76.9558;
      this.label = 'C';
      this.markers = [
        {
          lat: 11.014855,
          lng: 76.964711,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Aquatic Industries',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '33 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.0157749,
          lng: 76.9555643,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/reds.gif',
          info: 'Kattupalayam',
          type: 'Water Pressure Sensor',
          previousReading: '29 C',
          currentReading: '30 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.011190,
          lng: 76.962854,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Tamil Nadu',
          type: 'Temperature Sensor',
          previousReading: '33 C',
          currentReading: '32 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.013846,
          lng: 76.952070,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Senjeri Ayyampalayam',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '33 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.016544,
          lng: 76.946208,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Raja Nagar',
          type: 'Water Pressure Sensor',
          previousReading: '30 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.012941,
          lng: 76.941101,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/reds.gif',
          info: 'Tamil Nadu',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '33 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.023052,
          lng: 76.944806,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: '678582',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '33 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.019912,
          lng: 76.960122,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Tamil Nadu',
          type: 'Water Pressure Sensor',
          previousReading: '35 C',
          currentReading: '35 C',
          time: this.timeVar,
          status: 'Under Maintanace'
        },
        {
          lat: 11.022729,
          lng: 76.976007,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/reds.gif',
          info: 'Bharathi Nagar',
          type: 'Air Pressure Sensor',
          previousReading: '21 C',
          currentReading: '32 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.006420,
          lng: 76.968842,
          label: 'C',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Mathiyalagan Nagar',
          type: 'Air Pressure Sensor',
          previousReading: '28 C',
          currentReading: '25 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.000696,
          lng: 76.959726,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/reds.gif',
          info: 'Bharathi Nagar',
          type: 'Air Pressure Sensor',
          previousReading: '21 C',
          currentReading: '32 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.002405,
          lng: 76.956469,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Tamil Nadu',
          type: 'Water Pressure Sensor',
          previousReading: '35 C',
          currentReading: '35 C',
          time: this.timeVar,
          status: 'Under Maintanace'
        },
        {
          lat: 11.005812,
          lng: 76.943309,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/reds.gif',
          info: 'Bharathi Nagar',
          type: 'Air Pressure Sensor',
          previousReading: '21 C',
          currentReading: '32 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.023812,
          lng: 78.961953,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Anaiyur',
          type: 'Water Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.023833,
          lng: 78.962254,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Anaiyur',
          type: 'Water Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Working'
        }
      ];
      this.GS.getDeviceInfo('Coimbatore').subscribe(data => {
        // console.log('device infoooooo ', JSON.stringify(data));
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Madurai') {
      this.lat = 9.9252;
      this.lng = 78.1198;
      this.label = 'M';
      this.zoom = 10;
      this.markers = [
        {
          lat: 9.9662,
          lng: 78.1123,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Anaiyur',
          type: 'Water Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 9.9082,
          lng: 78.2008,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Tamil Nadu',
          type: 'Air Pressure Sensor',
          previousReading: '29 C',
          currentReading: '26 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 9.9225,
          lng: 78.30947,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Veppadappu',
          type: 'Air Pressure Sensor',
          previousReading: '39 C',
          currentReading: '36 C',
          time: this.timeVar,
          status: 'Under Maintanace'
        },
        {
          lat: 9.9001,
          lng: 78.4422,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Puduppatti',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '32 C',
          time: this.timeVar,
          status: 'Under Maintanace'
        },
        {
          lat: 9.9316,
          lng: 78.5023,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Okkurpudur',
          type: 'Water Pressure Sensor',
          previousReading: '31 C',
          currentReading: '30 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat:
            9.9441,
          lng: 78.6561,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Tamil Nadu',
          type: 'Air Pressure Sensor',
          previousReading: '23 C',
          currentReading: '30 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 9.9261,
          lng: 78.7215,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Velayuthapattanam',
          type: 'Water Pressure Sensor',
          previousReading: '28 C',
          currentReading: '30 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 9.9431,
          lng: 78.80009,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Tamilnadu',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 9.9079479,
          lng: 78.1129516,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Asiya Apartments',
          type: 'Water Pressure Sensor',
          previousReading: '29 C',
          currentReading: '33 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 9.9057,
          lng: 78.0918,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Palangantham',
          type: 'Water Pressure Sensor',
          previousReading: '39 C',
          currentReading: '33 C',
          time: this.timeVar,
          status: 'Working'
        },

      ];
      this.GS.getDeviceInfo('Madurai').subscribe(data => {
        // console.log('device infoooooo ', JSON.stringify(data));
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Salem') {
      this.lat = 11.6643;
      this.lng = 78.1460;
      this.label = 'S'
      this.zoom = 10;
      this.markers = [
        {
          lat: 11.65373,
          lng: 78.62494,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Kondakal Rd',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.853763,
          lng: 78.80003,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Kallipattu',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.963574,
          lng: 78.89954363,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Serapattu Main Rd',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.2436743,
          lng: 78.39797,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Semmedu - Kovilur Rd',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },

        {
          lat: 11.436478,
          lng: 78.45622,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Vadakkadu - Alangudi Road',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.53625,
          lng: 78.42617,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Muthuruttu',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.76352,
          lng: 78.195372,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Hill Church Road',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.353764,
          lng: 78.5346725,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'T.Kanapadi R.F',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.826376,
          lng: 78.4255367,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Kottimadava',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },

        {
          lat: 11.5363265,
          lng: 78.66427,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Attur - Perambalur Rd',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Salem').subscribe(data => {
        // console.log('device infoooooo ', JSON.stringify(data));
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Thanjavur') {
      this.lat = 10.7870;
      this.lng = 79.1378;
      this.zoom = 10;
      this.markers = [
        {
          lat: 10.7832,
          lng: 79.3261,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Ammapettai',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.8963,
          lng: 79.1944,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Almadina street',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.3413,
          lng: 79.3796,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Kaaliyaar St Road',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.7373,
          lng: 79.1366,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Nanjikottai Rd',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 10.8971,
          lng: 79.1284,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Thingalur',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 10.9617,
          lng: 79.3881,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Manthai - Karuppur Main Rd',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.3797,
          lng: 78.8208,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Manapparai-Pudukottai Rd',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.0407445,
          lng: 79.06874249999998,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Paluvur',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.6568,
          lng: 79.1012,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'North St',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.9789,
          lng: 79.4057,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Pillayam Pettai',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Thanjavur').subscribe(data => {
        // console.log('device infoooooo ', JSON.stringify(data));
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Tiruchirapalli') {
      this.zoom = 10;
      this.lat = 10.7905;
      this.lng = 78.7047;
      this.markers = [
        {
          lat: 10.7351391,
          lng: 78.73061580000001,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Gundur',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.7506,
          lng: 78.7764,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Thiruverumbur Rd',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.718239,
          lng: 78.43562,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Padiripatti',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.763287,
          lng: 78.6592,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Madurai Main Rd',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 10.9548837,
          lng: 78.44393200000002,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Musiri',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 10.6071579,
          lng: 78.42144439999993,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Manapparai',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.75263,
          lng: 78.94739,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Avarampatti',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.71111,
          lng: 78.99999,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Kurumpundi',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.9229564,
          lng: 78.74053570000001,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Samayapuram',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.763333,
          lng: 78.46922,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Puthur',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },

      ];
      this.GS.getDeviceInfo('Tiruchirapalli').subscribe(data => {
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Vellore') {
      this.zoom = 10;
      this.lat = 12.9165;
      this.lng = 79.1325;
      this.markers = [
        {
          lat: 12.904441,
          lng: 79.31915759999993,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Arcot',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 12.7903613,
          lng: 78.71660840000004,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Ambur',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 12.9657098,
          lng: 79.16755149999995,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Brahmapuram',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 12.7522395,
          lng: 79.1477979,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Kannamangalam',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 12.9447361,
          lng: 78.87090190000004,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Gudiyattam',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 12.9048206,
          lng: 79.32611399999996,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Kalavai Road',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 12.8443968,
          lng: 79.01364749999993,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Pulimedu',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 12.9767298,
          lng: 79.36485960000005,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Ammoor',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 12.8286068,
          lng: 79.3066953,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Timiri',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 12.7655175,
          lng: 78.9281896,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Muthukkumaran Malai Murgan Temple ( 70 years old )',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Vellore').subscribe(data => {
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Tirunelveli') {
      this.zoom = 10;
      this.lat = 8.7139;
      this.lng = 77.7567;
      this.markers = [
        {
          lat: 8.6433793,
          lng: 77.78668749999997,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Sivanthipatti',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 8.6679306,
          lng: 77.58773180000003,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Pattamadai',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 8.788720699999999,
          lng: 77.75259000000005,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Rajavallipuram',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 8.7218053,
          lng: 77.6583511,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Pettai',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 8.545382,
          lng: 77.76826410000001,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Moolaikaraipatti',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 8.5552454,
          lng: 77.61208520000002,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Devanallur Water Bridge',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 8.735787799999999,
          lng: 77.51894579999998,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Mukkudal',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 8.855005499999999,
          lng: 77.65218070000003,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Manur',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 8.8646356,
          lng: 77.49600780000003,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Alangulam',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 8.6634393,
          lng: 77.48202779999997,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Moolachi',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Tirunelveli').subscribe(data => {
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Tiruppur') {
      this.zoom = 10;
      this.lat = 11.1085;
      this.lng = 77.3411;
      this.markers = [
        {
          lat: 11.1085242,
          lng: 77.34106559999998,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Amaravathi Palayam Rd',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.1914474,
          lng: 77.26888209999993,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Avinashi',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.1984504,
          lng: 77.42286250000006,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Chengapally',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.9996667,
          lng: 77.28074229999993,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Palladam',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 10.3050361,
          lng: 76.17547569999999,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Padiyur',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.091599,
          lng: 77.4364706,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Branch 2 Tiruppur',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.0870641,
          lng: 77.1858995,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Somanur',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 10.9399263,
          lng: 77.44151179999994,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Koduvai',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 10.7329354,
          lng: 77.52181270000005,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Dharapuram',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.0053238,
          lng: 77.56098429999997,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Kangayam',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Tiruppur').subscribe(data => {
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Thoothukudi') {
      this.zoom = 10;
      this.lat = 8.7642;
      this.lng = 78.1348;
      this.markers = [
        {
          lat: 8.7624747,
          lng: 78.17639370000006,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Tuticorin Themal Power Station',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 8.6284843,
          lng: 78.03951489999997,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Valavallan',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 8.4963081,
          lng: 78.12508479999997,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Tiruchendur',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 8.9321465,
          lng: 78.03273769999998,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Panchalankurichi',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 8.8232095,
          lng: 77.99206549999997,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Umarikottai',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 10.379663,
          lng: 78.82084540000005,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Pudukottai',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 9.127882699999999,
          lng: 78.14496689999999,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Vaippar',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 9.078824,
          lng: 78.36288949999994,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Vembar',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 9.167084200000001,
          lng: 78.44393200000002,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Sayalgudi',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 9.2191566,
          lng: 78.71077309999998,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Ervadi',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Thoothukudi').subscribe(data => {
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
    else if (x === 'Erode') {
      this.zoom = 10;
      this.lat = 11.3410;
      this.lng = 77.7172;
      this.markers = [
        {
          lat: 11.4469008,
          lng: 77.68399610000006,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Bhavani',
          type: 'Water Pressure Sensor',
          previousReading: '23 C',
          currentReading: '24 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.4504099,
          lng: 77.43003569999996,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Gobichettipalayam',
          type: 'Air Pressure Sensor',
          previousReading: '25 C',
          currentReading: '19 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.37825,
          lng: 77.896927,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Tiruchengode',
          type: 'Air Pressure Sensor',
          previousReading: '27 C',
          currentReading: '22 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.3131883,
          lng: 77.78647769999998,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Kokkarayanpettai ',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '28 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.1697857,
          lng: 77.4514269,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/red.png',
          info: 'Utukuli ',
          type: 'Water Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Not Working'
        },
        {
          lat: 11.2745621,
          lng: 77.58260339999993,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Perundurai',
          type: 'Air Pressure Sensor',
          previousReading: '33 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.0788297,
          lng: 77.88674600000002,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Kodumudai',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
        {
          lat: 11.1627122,
          lng: 77.59632710000005,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Chennimalai',
          type: 'Water Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.2393791,
          lng: 77.85316510000007,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/yellow.png',
          info: 'Pasur',
          type: 'Air Pressure Sensor',
          previousReading: '32 C',
          currentReading: '29 C',
          time: this.timeVar,
          status: 'Under Maintanance'
        },
        {
          lat: 11.1867508,
          lng: 77.77381630000002,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Elumathur',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '27 C',
          time: this.timeVar,
          status: 'Working'
        },
      ];
      this.GS.getDeviceInfo('Erode').subscribe(data => {
        this.deviceArray = data[0];
        this.single = [
          {
            "name": "Working",
            "value": this.deviceArray.iotWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.iotStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.iotUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.iotDeployed
          }
        ];
        this.single1 = [
          {
            "name": "Working",
            "value": this.deviceArray.wiredWorking
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.wiredStopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.wiredUnderMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.wiredDeployed
          }
        ];
        this.single2 = [
          {
            "name": "Working",
            "value": this.deviceArray.working
          },
          {
            "name": "Stopped",
            "value": this.deviceArray.stopped
          },
          {
            "name": "Maintanance",
            "value": this.deviceArray.underMaintanance
          },
          {
            "name": "Deployed",
            "value": this.deviceArray.deployed
          }
        ];
      });
    }
  }

  markers: marker[] = [
    {
      lat: 11.0168,
      lng: 76.9558,
      label: 'C',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      previousReading: '',
      currentReading: '',
      time: this.timeVar,
      status: '',
      collectionBill: '',
      bandwidth: ''
    }
  ];
  parkingMarkers: parkingMarker[] = [
    {
      lat: 11.0168,
      lng: 76.9558,
      label: 'C',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      previousReading: '',
      currentReading: '',
      time: this.timeVar,
      status: '',
      collectionBill: '',
      bandwidth: '',
      typeOfParking: '',
      fee: '',
      discounts: '',
      id: '',
      cityName: ''
    }
  ];
  poleMarkers: poleMarker[] = [
    {
      lat: 11.0168,
      lng: 76.9558,
      label: 'C',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      previousReading: '',
      currentReading: '',
     
      status: '',
      
      
      id: '',
      cityName: '',
      lightStatus: '',
    }
  ];
  waterMarkers: waterMarker[] = [
    {
      lat: 11.0168,
      lng: 76.9558,
      label: 'C',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      previousReading: '',
      currentReading: '',
     
      status: '',
      
      
      id: '',
      cityName: '',
      
      collectionBill: '',
    }
  ];
  wasteMarkers: wasteMarker[] = [
    {
      lat: 11.0168,
      lng: 76.9558,
      label: 'C',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      previousReading: '',
      currentReading: '',
     
      status: '',
      
      
      id: '',
      cityName: '',
      
      
    }
  ];
  transportMarkers: transportMarker[] = [
    {
      lat: 11.0168,
      lng: 76.9558,
      label: 'C',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      previousReading: '',
      currentReading: '',
     
      status: '',
      
      
      id: '',
      cityName: '',
      
      
    }
  ];
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: string;
  type: string;
  previousReading: any;
  currentReading: any;
  time: any;
  status: string;
  collectionBill?: any;
  bandwidth?: string;
}
interface parkingMarker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: string;
  type: string;
  previousReading: any;
  currentReading: any;
  time: any;
  status: string;
  collectionBill?: any;
  bandwidth?: string;
  typeOfParking: any;
  fee: any;
  discounts: any;
  id: any;
  cityName: any;
}
interface poleMarker {
  
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: string;
  type: string;
  previousReading: any;
  currentReading: any;
  
  status: string;
  
  
  id: any;
  cityName: any;
  lightStatus:any;
}
interface waterMarker {
  
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: string;
  type: string;
  previousReading: any;
  currentReading: any;
  status: string;
  id: any;
  cityName: any;
  // lightStatus:any;
  collectionBill: any;
}
interface wasteMarker {
  
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: string;
  type: string;
  previousReading: any;
  currentReading: any;
  status: string;
  id: any;
  cityName: any;
  // lightStatus:any;
  
}
interface transportMarker {
  
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: string;
  type: string;
  previousReading: any;
  currentReading: any;
  status: string;
  id: any;
  cityName: any;
  // lightStatus:any;
  
}
$(window).scroll(function () {
  var scroll = $(window).scrollTop();

  //>=, not <=
  if (scroll >= 100) {
    //clearHeader, not clearheader - caps H
    $("#digitalDashboard").addClass("darkHeader");
  }
  if (scroll <= 10) {
    //clearHeader, not clearheader - caps H
    $("#digitalDashboard").removeClass("darkHeader");
  }
})