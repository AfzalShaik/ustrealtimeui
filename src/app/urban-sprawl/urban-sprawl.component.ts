import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
import { variable } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-urban-sprawl',
  templateUrl: './urban-sprawl.component.html',
  styleUrls: ['./urban-sprawl.component.css'],
  providers : [GraphsService]
})
export class UrbanSprawlComponent implements OnInit {
  options: any;
  newData:any =[]
  data111:any=[]
  view: any[] = [700, 400];
  constructor(private GS: GraphsService, private route: ActivatedRoute, private router: Router) { }
  ngOnInit() {
    console.log('');
    this.GS.getregisterData().subscribe(data => {
    this.newData =data;
    var cc: any = [];
    var allCount: number;
    var cc1: any = [];
    var allCount1: number; 

     this.newData.map((index)=>{
     
       allCount = cc.push(index.registrations);
    })
    this.newData.map((index)=>{
     
      allCount1 = cc1.push(index.value);
   })
   console.log( "klklkl"  + cc1)
    this.options = {
      title: { text: '' },
      xAxis: {
        categories: cc,
        max:6
    },
    yAxis: {
      title: { text: 'Value( In Lakhs)' }
    },
    tooltip:{
      borderWidth: 0,
      shadow: false,
      style: {
        padding: 0
      },
              formatter:function(){
              return 'INR '+ this.y;
              }
          },
			series: [{
				name: 'No. of Registartions',
				data: cc1,
				tooltip: {
					valueDecimals: 2
				},
				allowPointSelect: true
			}]
    };
  } )
  }

}
