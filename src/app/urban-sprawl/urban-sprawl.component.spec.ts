import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UrbanSprawlComponent } from './urban-sprawl.component';

describe('UrbanSprawlComponent', () => {
  let component: UrbanSprawlComponent;
  let fixture: ComponentFixture<UrbanSprawlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UrbanSprawlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UrbanSprawlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
