import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
declare var $: any;
@Component({
  selector: 'app-air-purity',
  templateUrl: './air-purity.component.html',
  styleUrls: ['./air-purity.component.css']
})
export class AirPurityComponent implements OnInit {
// private chart: AmChart;

timeVar:any;
options: any;
severity1: boolean;
deficientVar: boolean;
excessVar: boolean;
moderateVar: boolean;
goodVar: boolean;
severeVar: boolean;
noiseVar: boolean;
markers2:any[]
oVar: boolean;
noVar: boolean;
soVar: boolean;
coVar: boolean;
severity: any;
type: any;
city: any;
area: any;
selectedCityName: any;
smartCityInfo: any;
todayDate: any;
zoom: any = 8;
zoom1: any = 8;
zoomview:number;
latview:number;
lngview:number;
// initial center position for the map
lat: number = 11.3272889;
lng: number = 78.07734;
lat1: number = 11.3272889;
lng1: number = 78.07734;
latParking: number = 11.3272889;
lngParking: number = 78.07734;
zoomControl: boolean = true;
clickedMarker(label: any, index: number) {
  // this.router.navigate(['parent/environment', label.city, this.airPurityStatus]);
  console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
  this.area = label.label;
  this.city = label.city;
  this.type = label.type;
  this.severity = label.severity;
  // debugger;
  if (label.type == 'Severe') {
    this.severeVar = true;
    this.goodVar = false;
    this.moderateVar = false;
  } else if (label.type == "Good") {
    this.goodVar = true;
    this.severeVar = false;
    this.moderateVar = false;
  } else if (label.type == "Moderate") {
    this.moderateVar = true;
    this.goodVar = false;
    this.severeVar = false;
  }
  $('#myModal').modal('show');
    if (this.city == 'Coimbatore') {
      this.zoomview = 14;
      this.latview = 11.0168;
      this.lngview = 76.9558;
      this.markers2 = [
        {
          lat: 11.014855,
          lng: 76.964711,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Aquatic Industries',
          type: 'Air Pressure Sensor',
          previousReading: '31 C',
          currentReading: '33 C',
          status: 'Working',
          co2: '43.22',
          so2: '32.01',
          no2: '27.01',
          o2: '34.01',
          noise: '32.22'
        },
        {
          lat: 11.0157749,
          lng: 76.9555643,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/reds.gif',
          info: 'Kattupalayam',
          type: 'Water Pressure Sensor',
          previousReading: '29 C',
          currentReading: '30 C',
          status: 'Not Working',
          co2: '23.22',
          so2: '43.11',
          no2: '31.3',
          o2: '44.22',
          noise: '43.22'
        },
        {
          lat: 11.011190,
          lng: 76.962854,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/green.png',
          info: 'Tamil Nadu',
          type: 'Temperature Sensor',
          previousReading: '33 C',
          currentReading: '32 C',
          status: 'Working',
          co2: '24.11',
          so2: '19.33',
          no2: '45.11',
          o2: '54.01',
          noise: '55.11'
        },
        {
					lat: 11.013846,
					lng: 76.952070,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Senjeri Ayyampalayam',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					status: 'Under Maintanance',
					co2: '11.22',
					so2: '26.34',
					no2: '37.11',
					o2: '29.11',
					noise: '41.23'
				},
				{
					lat: 11.016544,
					lng: 76.946208,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Raja Nagar',
					type: 'Water Pressure Sensor',
					previousReading: '30 C',
					currentReading: '29 C',
					status: 'Working',
					co2: '39.11',
					so2: '22.11',
					no2: '24.11',
					o2: '27.11',
					noise: '29.11'
				},
				{
					lat: 11.012941,
					lng: 76.941101,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Tamil Nadu',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					status: 'Not Working',
					co2: '39.11',
					so2: '31.11',
					no2: '41.11',
					o2: '51.11',
					noise: '43.11'
				},
				{
					lat: 11.023052,
					lng: 76.944806,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: '678582',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					status: 'Working',
					co2: '44.44',
					so2: '22.32',
					no2: '54.22',
					o2: '49.11',
					noise: '12.22'
				},
				{
					lat: 11.019912,
					lng: 76.960122,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Tamil Nadu',
					type: 'Water Pressure Sensor',
					previousReading: '35 C',
					currentReading: '35 C',
					status: 'Under Maintanace',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '32.22'
				},
				{
					lat: 11.022729,
					lng: 76.976007,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Bharathi Nagar ',
					type: 'Air Pressure Sensor',
					previousReading: '21 C',
					currentReading: '32 C',
					status: 'Not Working',
					co2: '23.11',
					so2: '33.22',
					no2: '29.1',
					o2: '40.11',
					noise: '51.11'
				},
				{
					lat: 11.006420,
					lng: 76.968842,
					label: 'C',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Mathiyalagan Nagar',
					type: 'Air Pressure Sensor',
					previousReading: '28 C',
					currentReading: '25 C',
					status: 'Working',
					co2: '34.11',
					so2: '41.22',
					no2: '31.11',
					o2: '32.11',
					noise: '39.11'
				},
				{
					lat: 11.000696,
					lng: 76.959726,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Mathiyalagan Nagar',
					type: 'Air Pressure Sensor',
					previousReading: '21 C',
					currentReading: '32 C',
					status: 'Not Working',
					co2: '11.21',
					so2: '31.11',
					no2: '42.22',
					o2: '24.01',
					noise: '32.22'
				},
				{
					lat: 11.002405,
					lng: 76.956469,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Tamil Nadu',
					type: 'Water Pressure Sensor',
					previousReading: '35 C',
					currentReading: '35 C',
					status: 'Under Maintanace',
					co2: '31.11',
					so2: '52.11',
					no2: '31.01',
					o2: '42.11',
					noise: '32.22'
				},
				{
					lat: 11.005812,
					lng: 76.943309,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Bharathi Nagar',
					type: 'Air Pressure Sensor',
					previousReading: '21 C',
					currentReading: '32 C',
					status: 'Not Working',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '32.22'
				},
				{
					lat: 11.023812,
					lng: 78.961953,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Anaiyur',
					type: 'Water Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					status: 'Working',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '25.22'
				},
				{
					lat: 11.023833,
					lng: 78.962254,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Anaiyur',
					type: 'Water Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				}
      ]
    }
     else if (this.city == 'Tiruppur') {
      this.zoomview = 14;
      this.latview = 11.1085;
      this.lngview = 77.3411;
      
			this.markers2=[	{
					lat: 11.109579, 
					lng: 77.336605,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Esi Hospital',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.107179, 
					lng: 77.339501,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Khaderpet Flea Market Branded X Port Surplus',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.111749, 
					lng: 77.346655,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Zaithoon Masjid',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.112527, 
					lng: 77.340789,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'TIRUPUR NORTH POST OFFICE',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.115791, 
					lng: 77.336154,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'AG Church',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.114675, 
					lng: 77.347591,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Roopa Nursing Home',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.118991, 
					lng: 77.344544,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Andipalayam Common Effluent Treatment Plant Pvt Ltd',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.121897, 
					lng: 77.341604,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'SE OFFICE /TNEB/TIRUPUR',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				}]
    }
    else if (this.city == 'Thoothukudi') {
			this.latview = 8.7642;
			this.lngview = 78.1348;
			this.zoomview = 14;
			this.markers2 = [
				{
					lat: 8.761602, 
					lng: 78.127213,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Krishna & Co.',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.764168, 
					lng: 78.134830,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Elampuvanam VOC Rd',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.759969, 
					lng: 78.130946,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Abiramee Timber Company',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.766925, 
					lng: 78.135409,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Sethu Madhav Sea Shell Exports',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.764677, 
					lng: 78.138607,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Godown Premises VOC Rd',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.765483, 
					lng: 78.131483,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Bharat Petroleum NH-45B',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.763935, 
					lng: 78.137856,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Industrial Mineral Co Tuticorin',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.760987, 
					lng: 78.146524,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Sri Santhana Mariamman Thirukoil',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
        }]
       } else if(this.city == 'Erode'){
      this.zoomview = 10;
      this.latview =11.3410,
      this.lngview =77.7172;
      this.markers2 = [
        		{
        			lat: 11.4469008,
        			lng: 77.68399610000006,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/green.png',
        			info: 'Bhavani',
        			type: 'Water Pressure Sensor',
        			previousReading: '23 C',
        			currentReading: '24 C',
        			time: this.timeVar,
        			status: 'Working',
        			co2: '43.22'
        		},
        		{
        			lat: 11.4504099,
        			lng: 77.43003569999996,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/green.png',
        			info: 'Gobichettipalayam',
        			type: 'Air Pressure Sensor',
        			previousReading: '25 C',
        			currentReading: '19 C',
        			time: this.timeVar,
        			status: 'Working',
        			co2: '43.22'
        		},
        		{
        			lat: 11.37825,
        			lng: 77.896927,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/yellow.png',
        			info: 'Tiruchengode',
        			type: 'Air Pressure Sensor',
        			previousReading: '27 C',
        			currentReading: '22 C',
        			time: this.timeVar,
        			status: 'Under Maintanance',
        			co2: '43.22'
        		},
        		{
        			lat: 11.3131883,
        			lng: 77.78647769999998,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/red.png',
        			info: 'Kokkarayanpettai ',
        			type: 'Air Pressure Sensor',
        			previousReading: '31 C',
        			currentReading: '28 C',
        			time: this.timeVar,
        			status: 'Not Working'
        		},
        		{
        			lat: 11.1697857,
        			lng: 77.4514269,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/red.png',
        			info: 'Utukuli ',
        			type: 'Water Pressure Sensor',
        			previousReading: '33 C',
        			currentReading: '27 C',
        			time: this.timeVar,
        			status: 'Not Working'
        		},
        		{
        			lat: 11.2745621,
        			lng: 77.58260339999993,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/green.png',
        			info: 'Perundurai',
        			type: 'Air Pressure Sensor',
        			previousReading: '33 C',
        			currentReading: '27 C',
        			time: this.timeVar,
        			status: 'Working'
        		},
        		{
        			lat: 11.0788297,
        			lng: 77.88674600000002,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/green.png',
        			info: 'Kodumudai',
        			type: 'Water Pressure Sensor',
        			previousReading: '32 C',
        			currentReading: '27 C',
        			time: this.timeVar,
        			status: 'Working'
        		},
        		{
        			lat: 11.1627122,
        			lng: 77.59632710000005,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/yellow.png',
        			info: 'Chennimalai',
        			type: 'Water Pressure Sensor',
        			previousReading: '32 C',
        			currentReading: '29 C',
        			time: this.timeVar,
        			status: 'Under Maintanance'
        		},
        		{
        			lat: 11.2393791,
        			lng: 77.85316510000007,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/yellow.png',
        			info: 'Pasur',
        			type: 'Air Pressure Sensor',
        			previousReading: '32 C',
        			currentReading: '29 C',
        			time: this.timeVar,
        			status: 'Under Maintanance'
        		},
        		{
        			lat: 11.1867508,
        			lng: 77.77381630000002,
        			label: '',
        			draggable: false,
        			iconUrl: '../assets/images/green.png',
        			info: 'Elumathur',
        			type: 'Air Pressure Sensor',
        			previousReading: '31 C',
        			currentReading: '27 C',
        			time: this.timeVar,
        			status: 'Working'
        		},
        	];
    }
    else if(this.city == 'Tiruchirappalli'){
      this.zoomview = 14;
      this.latview =  10.7905,
      this.lngview =78.7047;
      this.markers2 = [
				{
					lat: 10.809026304002867,
					lng: 79.1070556640625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Ramanathapuram Chief',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.695695224886434,
					lng: 79.1455078125,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Perumbur IInd Sethi',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.698394077836666,
					lng: 79.0191650390625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: '	Kurumpundi',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.683550089556267,
					lng: 79.11666870117188,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Kurungulam Keelpathi',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.676802582247122,
					lng: 79.22103881835938,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Alivoikkal',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.710538618820474,
					lng: 79.24850463867188,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Raghavambalpuram Part',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:10.73212771142289,
					lng: 79.21279907226562,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Manapparai-Pudukottai Rd',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				}
    ]
    }
    else if(this.city == 'Thanjavur'){
      this.zoomview = 14;
      this.latview =  10.7870,
      this.lngview =79.1378;
      this.markers2 = [
				{
					lat: 10.809026304002867,
					lng: 79.1070556640625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Ramanathapuram Chief',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.695695224886434,
					lng: 79.1455078125,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Perumbur IInd Sethi',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.698394077836666,
					lng: 79.0191650390625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: '	Kurumpundi',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.683550089556267,
					lng: 79.11666870117188,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Kurungulam Keelpathi',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.676802582247122,
					lng: 79.22103881835938,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Alivoikkal',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.710538618820474,
					lng: 79.24850463867188,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Raghavambalpuram Part',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:10.73212771142289,
					lng: 79.21279907226562,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Manapparai-Pudukottai Rd',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				}
    ]
    }
    else if(this.city == 'Madurai'){
      this.zoomview = 10;
      this.latview =   9.9252,
      this.lngview =78.1198;
      this.markers2 = [
				{
					lat: 9.924518,
					lng: 78.123145,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: ' Yanaikkal Statue',
					type: 'Water Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.925270,
					lng: 78.119316,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Lochart Gap Point',
					type: 'Air Pressure Sensor',
					previousReading: '29 C',
					currentReading: '26 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.927003,
					lng: 78.131740,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Government Rajaji Hospital',
					type: 'Air Pressure Sensor',
					previousReading: '39 C',
					currentReading: '36 C',
					time: this.timeVar,
					status: 'Under Maintanace',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.926031,
					lng: 78.120990,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Annamalai Theatre',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '32 C',
					time: this.timeVar,
					status: 'Under Maintanace',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.926390,
					lng: 78.5023,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Madurai Sandhai',
					type: 'Water Pressure Sensor',
					previousReading: '31 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:9.932097,
					lng: 78.124037,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Sri Meenakshi Temple',
					type: 'Air Pressure Sensor',
					previousReading: '23 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.927468,
					lng: 78.114552,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'ISKCON Madurai',
					type: 'Water Pressure Sensor',
					previousReading: '28 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.934718,
					lng: 78.119359,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Sellur VAO Office',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				}]
    }
    else if(this.city == 'Salem'){
      this.zoomview = 10;
      this.latview = 11.6643,
      this.lngview =78.1460
      this.markers2 = [
				{
					lat: 11.662029,
					lng: 78.139207,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Sri Vinayagar Mariamman Temple',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.662828,
					lng: 78.148155,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'IDBI Bank K T Towers Omlur Main Road ',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
		
				},
				{
					lat: 11.667409,
					lng: 78.149786,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Nirmal Skywin Mall',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.668649,
					lng: 78.152833,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'BSNL Salem - GM Office',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.672095,
					lng: 78.145688,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'ARMC IVF Fertility Centre ',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.677223,
					lng: 78.142276,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Fairlands Police Station',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.676193,
					lng: 78.137577,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'SalemThyrocare Lab ',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.681888,
					lng:78.146331,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Michael Church',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
			];
    }
 
 
  
}
mapClicked($event: MouseEvent) {
  console.log('helloooooooooo', $event);
}
mouseOver($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
  // alert("test")
  // this.zoom=11;
  this.area = label;
  this.city = city;
  this.type = type;
  this.severity = severity;
  if (type == 'Severe') {
    this.severeVar = true;
    this.goodVar = false;
    this.moderateVar = false;
  } else if (type == "Good") {
    this.goodVar = true;
    this.severeVar = false;
    this.moderateVar = false;
  } else if (type == "Moderate") {
    this.moderateVar = true;
    this.goodVar = false;
    this.severeVar = false;
  }

  console.log('overrrrrrrrrrrrr ', label, city, severity, type);
  if (gm.lastOpen != null) {
    gm.lastOpen.close();
  }

  gm.lastOpen = infoWindow;

  infoWindow.open();
}
markerDragEnd(m: marker, $event: MouseEvent) {
  console.log('dragEnd', m, $event);
}
  constructor(private serviceAssert:GetDigitalAssetType, private router: Router) {
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
   }

  ngOnInit() {
    
    this.todayDate = new Date();
    // window.location.reload();
    // this.onSelect({"name":"Coimbatore","value":30});
    this.coVar = false;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity = true;
    this.airPurityStatus = 'pm';
    // this.severity = false;
    this.severity1 = true;
    this.markers2 = [
      {
        lat: 11.014855,
        lng: 76.964711,
        label: '',
        draggable: false,
        iconUrl: '../assets/images/green.png',
        info: 'Aquatic Industries',
        type: 'Air Pressure Sensor',
        previousReading: '31 C',
        currentReading: '33 C',
        status: 'Working',
        co2: '43.22',
        so2: '32.01',
        no2: '27.01',
        o2: '34.01',
        noise: '32.22'
      },
      {
        lat: 11.0157749,
        lng: 76.9555643,
        label: '',
        draggable: false,
        iconUrl: '../assets/images/reds.gif',
        info: 'Kattupalayam',
        type: 'Water Pressure Sensor',
        previousReading: '29 C',
        currentReading: '30 C',
        status: 'Not Working',
        co2: '23.22',
        so2: '43.11',
        no2: '31.3',
        o2: '44.22',
        noise: '43.22'
      },
      {
        lat: 11.011190,
        lng: 76.962854,
        label: '',
        draggable: false,
        iconUrl: '../assets/images/green.png',
        info: 'Tamil Nadu',
        type: 'Temperature Sensor',
        previousReading: '33 C',
        currentReading: '32 C',
        status: 'Working',
        co2: '24.11',
        so2: '19.33',
        no2: '45.11',
        o2: '54.01',
        noise: '55.11'
      }
    ]
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '24.916666 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '31.9 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '20.36 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Thanjavur',
        severity: '35.12 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R', 
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruchirappalli',
        severity: '31.98 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '11.78 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruppur',
        severity: '30.39 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Erode',
        severity: '43.98 µg/m³',
        type: 'Severe',
      },
    ]
   
  }
  airPurityStatus:any;
  soClick(data) {
    this.airPurityStatus = data;
    this.soVar = true;
    this.severity1 = false;
    this.noVar = false;
    this.oVar = false;
    this.noiseVar = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '24.3 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '34.3 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '7.11 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thanjavur',
        severity: '3.98 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruchirappalli',
        severity: '40 µg/m³',
        type: 'Severe',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '7.76 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '7.14 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruppur',
        severity: '46 µg/m³',
        type: 'Severe',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '7.14 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Erode',
        severity: '24.3 µg/m³',
        type: 'Good',
      },
    ]
  }
  noClick(data) {
    this.airPurityStatus = data;
    this.noVar = true;
    this.soVar = false;
    this.severity1 = false;
    this.oVar = false;
    this.noiseVar = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '15.2 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '25.2 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '10.38 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thanjavur',
        severity: '22.31 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruchirappalli',
        severity: '52 µg/m³',
        type: 'Severe',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'vellore',
        severity: '20.48 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '10.29 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruppur',
        severity: '15.2 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '10.29 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Erode',
        severity: '35.2 µg/m³',
        type: 'Moderate',
      },
    ]
  }
  oClick(data) {
    this.airPurityStatus = data;
    this.oVar = true;
    this.noVar = false;
    this.soVar = false;
    this.severity1 = false;
    this.noiseVar = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '41 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Salem',
        severity: '42 µg/m³',
        type: 'Good',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '41.5 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thanjavur',
        severity: '38.15 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruchirappalli',
        severity: '11 µg/m³',
        type: 'Severe',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'vellore',
        severity: '19.99 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '46.54 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruppur',
        severity: '21 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '46.54 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Erode',
        severity: '11 µg/m³',
        type: 'Moderate',
      },
    ]
  }
  noiseClick(data) {
    this.airPurityStatus = data;
    this.noiseVar = true;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity1 = false;
    this.coVar = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Coimbatore',
        severity: '55.50 dBA',
        type: 'Moderate',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Salem',
        severity: '77.50 dBA',
        type: 'Severe',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Madurai',
        severity: '57.50 dBA',
        type: 'Moderate',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Thanjavur',
        severity: '77.50 dBA',
        type: 'Severe',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruchirappalli',
        severity: '57.50 dBA',
        type: 'Good',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Vellore',
        severity: '77.367 dBA',
        type: 'Severe',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tirunelveli',
        severity: '87.50 dBA',
        type: 'Severe',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruppur',
        severity: '57.50 dBA',
        type: 'Good',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '57.50 dBA',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Erode',
        severity: '57.50 dBA',
        type: 'Good',
      },
    ]
  }
  co2Click(data) {
    this.airPurityStatus = data;
    this.coVar = true;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    this.severity1 = false;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Coimbatore',
        severity: '760.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '660.0 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Madurai',
        severity: '760.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R', Thanjavur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Thanjavur',
        severity: '720.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tiruchirappalli',
        severity: '460.0 µg/m³',
        type: 'Good',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '275.0 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tirunelveli',
        severity: '880.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Tiruppur',
        severity: '760.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Thoothkudi',
        severity: '880.0 µg/m³',
        type: 'Severe',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Erode',
        severity: '600.0 µg/m³',
        type: 'Moderate',
      },
    ]
  }
 
  pmClick(data) {
    this.airPurityStatus = data;
    // alert(this.airPurityStatus)
    this.coVar = false;
    this.noiseVar = false;
    this.oVar = false;
    this.noVar = false;
    this.soVar = false;
    // this.severity = false;
    this.severity1 = true;
    this.markers = [
      {
        lat: 11.0168,
        lng: 76.9558,
        // label: 'R', Coimbatore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Coimbatore',
        severity: '24.916666 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.6643,
        lng: 78.1460,
        // label: 'R', Salem
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Salem',
        severity: '31.9 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 9.9252,
        lng: 78.1198,
        // label: 'R', madurai
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Madurai',
        severity: '20.36 µg/m³',
        type: 'Good',
      },
      {
        lat: 10.7870,
        lng: 79.1378,
        // label: 'R',
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Thanjavur',
        severity: '35.12 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 10.7905,
        lng: 78.7047,
        // label: 'R', 
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruchirappalli',
        severity: '31.98 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 12.9165,
        lng: 79.1325,
        // label: 'R', vellore
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Vellore',
        severity: '11.78 µg/m³',
        type: 'Good',
      },
      {
        lat: 8.7139,
        lng: 77.7567,
        // label: 'R', Tirunelveli
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Tirunelveli',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.1085,
        lng: 77.3411,
        // label: 'R', Tiruppur
        draggable: true,
        iconUrl: '../assets/images/yellow.png',
        city: 'Tiruppur',
        severity: '30.39 µg/m³',
        type: 'Moderate',
      },
      {
        lat: 8.7642,
        lng: 78.1348,
        // label: 'R', Thoothkudi
        draggable: true,
        iconUrl: '../assets/images/green.png',
        city: 'Thoothkudi',
        severity: '20.52 µg/m³',
        type: 'Good',
      },
      {
        lat: 11.3410,
        lng: 77.7172,
        // label: 'R', Erode
        draggable: true,
        iconUrl: '../assets/images/red.png',
        city: 'Erode',
        severity: '43.98 µg/m³',
        type: 'Severe',
      },
    ]
  //   if(this.city){
  //     if (this.city == 'Coimbatore') {
        
  //       this.zoomview = 10;
  //       this.latview = 11.0168;
  //       this.lngview = 76.9558;
  //     } else if (this.city == 'Tiruppur') {
  //       this.zoomview = 10;
  //       this.latview = 11.1085;
  //       this.lngview = 77.3411;
  //     } else if(this.city == 'Erode'){
  //       this.zoomview = 10;
  //       this.latview =11.3410,
  //       this.lngview =77.7172;
  //     }
  //     else if(this.city == 'Tiruchirappalli'){
  //       this.zoomview = 10;
  //       this.latview =  10.7905,
  //       this.lngview =78.7047;
  //     }
  //     else if(this.city == 'Thanjavur'){
  //       this.zoomview = 10;
  //       this.latview =  10.7870,
  //       this.lngview =79.1378;
  //     }
  //     else if(this.city == 'Madurai'){
  //       this.zoomview = 10;
  //       this.latview =   9.9252,
  //       this.lngview =78.1198;
  //     }
  // }
}
  markers: marker[] = [

    {
      lat: 11.0168,
      lng: 76.9558,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '36',
      type: 'High',
    },
    {
      lat: 11.6643,
      lng: 78.1460,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/yellow.png',
      city: '',
      severity: '29',
      type: 'Moderate',
    },
    {
      lat: 9.9252,
      lng: 78.1198,
      // label: 'R', madurai
      draggable: true,
      iconUrl: '../assets/images/yellow.png',
      city: '',
      severity: '26',
      type: 'Moderate',
    },
    {
      lat: 10.7870,
      lng: 79.1378,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/green.png',
      city: '',
      severity: '24',
      type: 'Low',
    },
    {
      lat: 10.7905,
      lng: 78.7047,
      // label: 'R',
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '40',
      type: 'High',
    },
    {
      lat: 12.9165,
      lng: 79.1325,
      // label: 'R', vellore
      draggable: true,
      iconUrl: '../assets/images/green.png',
      city: '',
      severity: '24',
      type: 'Low',
    },
    {
      lat: 8.7139,
      lng: 77.7567,
      // label: 'R', Tirunelveli
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '40',
      type: 'Very High',
    },
    {
      lat: 11.1085,
      lng: 77.3411,
      // label: 'R', Tiruppur
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '46',
      type: 'Very High',
    },
    {
      lat: 8.7642,
      lng: 78.1348,
      // label: 'R', Thoothkudi
      draggable: true,
      iconUrl: '../assets/images/yellow.png',
      city: '',
      severity: '29',
      type: 'Moderate',
    },
    {
      lat: 11.3410,
      lng: 77.7172,
      // label: 'R', Erode
      draggable: true,
      iconUrl: '../assets/images/red.png',
      city: '',
      severity: '62',
      type: 'Very High',
    },
  ]
}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;

}