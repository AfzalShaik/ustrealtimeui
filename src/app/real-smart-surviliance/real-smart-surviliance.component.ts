import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
declare var $: any;
@Component({
  selector: 'app-real-smart-surviliance',
  templateUrl: './real-smart-surviliance.component.html',
  styleUrls: ['./real-smart-surviliance.component.css']
})
export class RealSmartSurvilianceComponent implements OnInit {
  zoom: number = 14;
  
  // initial center position for the map
  mapdata:any;
  lat: number = 13.0817597
  lng: number = 80.2694349;
  markers:any=[]
  srcdata:any;
  clickedMarker(label: string, lat: number, lng:number ) {
    // console.log(`clicked the marker: ${label || index}`)
    
    if(lat == 13.083933374053084){
    this.mapdata=true;
      $("#myModal").modal()
      this.srcdata ="https://s3.ap-south-1.amazonaws.com/smartcityvedio/rbsq2-otk3q.mp4"
    }else{
      this.mapdata=false;
      this.srcdata ="https://s3.ap-south-1.amazonaws.com/smartcityvedio/Garbage_result_mp4version.mp4"
      $("#myModal123").modal()
    }
   

  }
  mapClicked($event: MouseEvent) {
    console.log($event)
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  
  constructor() { }

  ngOnInit() {
    this.onload();
  }
  onload(){
    this.lat =13.0817597;
    this.lng =80.2694349;
    this.markers = [
      {
        lat: 13.083933374053084,
        lng: 80.2670826026939,
        iconUrl:'../assets/images/cctv.png',
        draggable: false
      },
      {
        lat: 13.077836521507338,
        lng:80.27402411307139,
        iconUrl:'../assets/images/cctv.png',
        draggable: false
      }
    ]
  }

}
