import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { FormGroup, FormArray, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-send-alert',
  templateUrl: './send-alert.component.html',
  styleUrls: ['./send-alert.component.css'],
  providers: [GraphsService]
})
export class SendAlertComponent implements OnInit {
  area: any = "Select area";
  phoneNumber: number
  dropdown: any = [];
  dropdown1: any = []
  name: any = '';
  phone: any = '';
  msg: any = '';
  portalDynamicForm: FormGroup;
  toppings = new FormControl();
  toppingList = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];

  constructor(private route: ActivatedRoute, private router: Router, private GS: GraphsService) { }


  ngOnInit() {
    this.getBulkData();
    this.portalDynamicForm = new FormGroup({
      'phoneNumber': new FormArray([])
    });
    const control = new FormControl('', Validators.required);
    (<FormArray>this.portalDynamicForm.get('phoneNumber')).push(control);
  }
  // splitBill(): void {
  //   this.portalDynamicForm = new FormGroup({
  //     'phoneNumber': new FormArray([])
  //   });
  //   const control = new FormControl('', Validators.required);
  //   (<FormArray>this.portalDynamicForm.get('phoneNumber')).push(control);
  // }

  addMorePeople(index) {
    const control = new FormControl('', Validators.required);
    (<FormArray>this.portalDynamicForm.get('phoneNumber')).push(control);
  }

  removeSplitBill(index) {
    const control = <FormArray>this.portalDynamicForm.controls['phoneNumber'];
    control.removeAt(index);
  }
  changeUrl: any = {};
  obj: any = {};
  iconUrl: any;
  updateUrl() {
    this.GS.getUrl().subscribe(data => {
      this.obj = data[0];
      this.obj.iconUrl = this.changeUrl.iconUrl;
      this.GS.updateUrl(this.obj).subscribe(data1 => {
      });
    });
  }
  submit(x) {

    let obj = {
      "message": this.msg,
      "dest": x
    }
    this.GS.sendSMS(obj).subscribe(data => {

      this.portalDynamicForm = new FormGroup({
        'phoneNumber': new FormArray([])
      });

    });
    this.msg = '';
  }
  submit1(p) {
    let phoneArray = [];
    this.GS.getBulkcontacts1('Surat').subscribe(data => {
      this.dropdown1 = data;
      // debugger;
      for(let i = 0; i<this.dropdown1.length; i++) {
        phoneArray.push(this.dropdown1[i].mobile)
      }
      var obj = {
        "message": p.value.message,
        "dest": phoneArray
      }
      // this.apiIntegration(obj);
      let msg = {
        "message" : p.value.message
      }
      this.GS.getSound3(msg).subscribe(sendData => {
        console.log('response send', sendData);
      });
      console.log(obj)
      this.GS.sendSMS(obj).subscribe(data => {
        p.reset();
      });
    })
   
   

  }
  apiIntegration(x) {
    // this.GS.getSound1().subscribe(data => {

    // });
    this.GS.getSound3(x).subscribe(data => {

    });
  }
  getBulkData() {

    this.GS.getBulkcontacts().subscribe(data => {
      // alert("test")
      this.dropdown = data;
    })
  }
  getplaceData(area) {

    this.GS.getBulkcontacts1('Surat').subscribe(data => {
      this.dropdown1 = data;
    })
  }
}
