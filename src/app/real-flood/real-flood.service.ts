import { Injectable } from '@angular/core';
import { AppURL } from '../../apiUrl';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import { Response } from '@angular/http/src/static_response';

@Injectable()
export class RealFloodService {

  constructor( private http: Http) { }
  getMapDataByCity(cityname ) {
    // alert(1)
   const city = cityname.toLowerCase();
    // return this.http.get(AppURL.api+'EnvironmentGraphs?filter={"where":{"cityname":"'+city+'"}}').map((res) => res);
    return this.http.get(AppURL.api+cityname).map((res:Response) => res.json());
    
  }

}
