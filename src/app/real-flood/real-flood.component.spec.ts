import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealFloodComponent } from './real-flood.component';

describe('RealFloodComponent', () => {
  let component: RealFloodComponent;
  let fixture: ComponentFixture<RealFloodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealFloodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealFloodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
