import { TestBed, inject } from '@angular/core/testing';

import { RealFloodService } from './real-flood.service';

describe('RealFloodService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RealFloodService]
    });
  });

  it('should be created', inject([RealFloodService], (service: RealFloodService) => {
    expect(service).toBeTruthy();
  }));
});
