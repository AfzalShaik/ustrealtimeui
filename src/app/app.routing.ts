
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {HomeComponentComponent} from './home-component/home-component.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { IcccComponent } from './iccc/iccc.component';
import { StateLevelComponent } from './state-level/state-level.component';
import { SmartParkingComponent } from './smart-parking/smart-parking.component';
import { AirPurityComponent } from './air-purity/air-purity.component';
import { WaterScadaComponent } from './water-scada/water-scada.component';
import { StateAllComponent } from './state-all/state-all.component';
import { IncidentDashboardComponent } from './incident-dashboard/incident-dashboard.component';
import { EnvironmentComponent } from './environment/environment.component';
import { DigitalAssetDashboardComponent } from './digital-asset-dashboard/digital-asset-dashboard.component';
import { ParkingComponent } from './parking/parking.component';
import { SmartPoleComponent } from './smart-pole/smart-pole.component';
import { SmartWaterComponent } from './smart-water/smart-water.component';
import { SmartWifiComponent } from './smart-wifi/smart-wifi.component';
import { SmartWasteComponent } from './smart-waste/smart-waste.component';
import { SmartTransportComponent } from './smart-transport/smart-transport.component';
import { AuthGuard } from './guards';
import { SendAlertComponent } from './send-alert/send-alert.component';
import { WaterSewageComponent } from './water-sewage/water-sewage.component';
import { StateTransportComponent } from './state-transport/state-transport.component';
import { IncidentListComponent } from './incident-list/incident-list.component';

import { PropertyTaxComponent } from './property-tax/property-tax.component';
import { BirthdeathComponent } from './birthdeath/birthdeath.component';
import { ParkingRealTimeComponent } from './parking-real-time/parking-real-time.component';
import { EnvironmentRealTimeComponent } from './environment-real-time/environment-real-time.component';
import { WasteRealTimeComponent } from './waste-real-time/waste-real-time.component';
import { RealWifiComponent } from './real-wifi/real-wifi.component';
import { RealPoleComponent } from './real-pole/real-pole.component';
import { RealTrafficComponent } from './real-traffic/real-traffic.component';
import { RealFloodComponent } from './real-flood/real-flood.component';
import { RealSmartSurvilianceComponent } from './real-smart-surviliance/real-smart-surviliance.component';
import { UrbanSprawlComponent } from './urban-sprawl/urban-sprawl.component';
const routing: Routes = [
        { path: '', component: HomeComponentComponent },
        // {path: 'dashboard', component:DashboardComponentComponent},
        {
            path: 'dashboard', component: DashboardComponentComponent,canActivate: [AuthGuard],
            children: [
              { path: 'iccc/:id', component: IcccComponent },
            //   { path: 'tabsArea/:id', component: TabsAreaComponent },
            //   { path: 'digitalAsset/:id', component: DigitalAssetDashboardComponent },
              { path: 'statlevel/:id', component: StateLevelComponent },
              { path: 'stateparking/:id', component: SmartParkingComponent },
              { path: 'stateair/:id', component: AirPurityComponent },
              { path: 'statewater/:id', component: WaterScadaComponent },
              { path: 'stateall/:id', component: StateAllComponent },
              { path: 'incident/:id', component: IncidentDashboardComponent },
              { path: 'environment/:id', component: EnvironmentComponent },
              { path: 'digital/:id', component: DigitalAssetDashboardComponent },
              { path: 'parking/:id', component: ParkingComponent },
              { path: 'pole/:id', component: SmartPoleComponent },
              { path: 'water/:id', component: SmartWaterComponent },
              { path: 'wifi/:id', component: SmartWifiComponent },
              { path: 'waste/:id', component: SmartWasteComponent },
              { path: 'transport/:id', component: SmartTransportComponent },
              { path: 'alert/:id', component: SendAlertComponent },
              { path: 'citywater/:id', component: WaterSewageComponent },
              { path: 'statetransport/:id', component: StateTransportComponent },
              { path: 'incidents/:id', component: IncidentListComponent },
              { path: 'propertytax/:id', component: PropertyTaxComponent},
              { path: 'birth/:id', component: BirthdeathComponent},
              { path: 'realParking/:id', component: ParkingRealTimeComponent},
              { path: 'realEnvironment/:id', component: EnvironmentRealTimeComponent},
              { path: 'realBin/:id', component: WasteRealTimeComponent},
              { path: 'realWifi/:id', component: RealWifiComponent},
              { path: 'realPole/:id', component: RealPoleComponent},
              { path: 'realFlood/:id', component: RealFloodComponent},
              { path: 'realTraffic/:id', component: RealTrafficComponent},
              { path: 'smartsurveiliance/:id', component: RealSmartSurvilianceComponent},
              { path: 'urbansprawl/:id', component: UrbanSprawlComponent},
            //   { path: 'environment/:id', component: EnvironmentComponent },
            //   { path: 'environment/:id/:status', component: EnvironmentComponent },
           
            ]
          }
    ];



export const RoutingModule: ModuleWithProviders = RouterModule.forRoot(routing);