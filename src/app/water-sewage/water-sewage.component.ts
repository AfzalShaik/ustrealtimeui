import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
// import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
declare var $: any;

@Component({
  selector: 'app-water-sewage',
  templateUrl: './water-sewage.component.html',
  styleUrls: ['./water-sewage.component.css']
})
export class WaterSewageComponent implements OnInit {
  options: any;
  severity1: boolean;
  deficientVar: boolean;
  excessVar: boolean;
  moderateVar: boolean;
  goodVar: boolean;
  severeVar: boolean;
  noiseVar: boolean;
  oVar: boolean;
  noVar: boolean;
  soVar: boolean;
  coVar: boolean;
  severity: any;
  type: any;
  city: any;
  area: any;
  selectedCityName: any;
  smartCityInfo: any;
  todayDate: any;
  // zoom: any = 8;
  zoom1: any = 10;
  // initial center position for the map
	// lat: number = 11.3272889;
  // lng: number = 78.07734;
  lat1: number = 11.0168;
  lng1: number = 76.9558;
  // latParking: number = 11.3272889;
  // lngParking: number = 78.07734;
  zoomControl: boolean = true;
  constructor(private serviceAssert:GetDigitalAssetType, private router: Router) { 
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
  }
  view: any[] = [590, 300];
  colorScheme4 = {
    domain: [ '#b87b06','#5ca026']
  };

  data111 = [
    {
      "name": "Ram Nagar",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 39
        },
        {
          "name": "Capacity",
          "value": 44
        },
      ]
    },
    {
      "name": "Malumichampatti",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 79
        },
        {
          "name": "Capacity",
          "value": 88
        },
      ]
    },
    {
      "name": "Karumathampatti",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 118
        },
        {
          "name": "Capacity",
          "value": 133
        },
      ]
    },
    {
      "name": "Palladam",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 158
        },
        {
          "name": "Capacity",
          "value": 177
        },
      ]
    },
    {
      "name": "Annur",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 197
        },
        {
          "name": "Capacity",
          "value": 223
        },
      ]
    },
    {
      "name": "Mettupalayam",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 266
        },
        {
          "name": "Capacity",
          "value": 237
        },
      ]
    },
    {
      "name": "Kattampatti",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 355
        },
        {
          "name": "Capacity",
          "value": 311
        },
      ]
    },
    {
      "name": "Gudalur",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 791
        },
        {
          "name": "Capacity",
          "value": 855
        },
      ]
    }
    
  ]
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';

  showDataLabel=true;

  ngOnInit() {
    this.todayDate = new Date();
  }
  clickedMarker1(label: any, index: number) {
    console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
    this.area = label.label;
    this.city = label.city;
    this.type = label.type;
    this.severity = label.severity;
    
  }
  mapClicked1($event: MouseEvent) {
    console.log('helloooooooooo', $event);
  }
  mouseOver1($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
    // debugger;
    this.area = label;
    this.city = city;
    this.type = type;
    this.severity = severity;
    if (this.type == 'Excess Suply') {
      this.excessVar = true;
      this.deficientVar = false;
    } else if (this.type == 'Deficient') {
      this.deficientVar = true;
      this.excessVar = false;
    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  markers1: marker1[] = [

    {
      lat: 11.0168,
      lng: 76.9558,
      label: '39KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '44KL',
      type: 'Excess Suply',
    },
    {
      lat: 10.919579453016539,
      lng: 76.98522320425059,
       label: '79KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '88KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.155459658173301,
      lng: 76.93853130971934,
       label: '118KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Coimbatore',
      severity: '133KL',
      type: 'Deficient',
    },
    {
      lat: 11.11234145988578,
      lng: 77.17748394643809,
      label: '158KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '177KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.243023777039845,
      lng: 77.10560548061426,
      label: '197KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '223KL',
      type: 'Excess Suply',
    },
    {
      lat: 10.995081702618185,
      lng: 77.28185406362559,
      label: '237KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '266KL',
      type: 'Excess Suply',
    },
    {
      lat: 10.750229023099982,
      lng: 77.02230206167246,
      label: '276KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Coimbatore',
      severity: '311KL',
      type: 'Deficient',
    },
    {
      lat: 10.814683387021708,
      lng: 77.15825787221934,
      label: '316KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Coimbatore',
      severity: '355KL',
      type: 'Deficient',
    },
  ]
}
interface marker1 {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}