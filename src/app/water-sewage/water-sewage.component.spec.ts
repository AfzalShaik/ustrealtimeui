import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WaterSewageComponent } from './water-sewage.component';

describe('WaterSewageComponent', () => {
  let component: WaterSewageComponent;
  let fixture: ComponentFixture<WaterSewageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WaterSewageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaterSewageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
