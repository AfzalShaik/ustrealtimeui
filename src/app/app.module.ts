import { BrowserModule, DomSanitizer} from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule } from './app.routing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './header-component/header-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';
import { HomeComponentComponent } from './home-component/home-component.component';
import { LoginComponentComponent } from './login-component/login-component.component';
import { DashboardComponentComponent } from './dashboard-component/dashboard-component.component';
import { IcccComponent } from './iccc/iccc.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { HttpModule } from '@angular/http';
import {HttpClientModule} from '@angular/common/http';
import { StateLevelComponent } from './state-level/state-level.component';
import { DigitalAssetDashboardComponent } from './digital-asset-dashboard/digital-asset-dashboard.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {GoogleChart} from './directives/angular2-google-chart.directive';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import {GetDigitalAssetType} from './digital-asset-dashboard/digital-asset-dashboard.services';
import { SmartParkingComponent } from './smart-parking/smart-parking.component';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AirPurityComponent } from './air-purity/air-purity.component';
import { WaterScadaComponent } from './water-scada/water-scada.component';
import { StateAllComponent } from './state-all/state-all.component';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { IncidentDashboardComponent } from './incident-dashboard/incident-dashboard.component';
import { EnvironmentComponent } from './environment/environment.component';
import { ParkingComponent } from './parking/parking.component';
import { SmartPoleComponent } from './smart-pole/smart-pole.component';
import { SmartWaterComponent } from './smart-water/smart-water.component';
import { SmartWifiComponent } from './smart-wifi/smart-wifi.component';
import { SmartWasteComponent } from './smart-waste/smart-waste.component';
import { SmartTransportComponent } from './smart-transport/smart-transport.component';
import { CookieService } from 'angular2-cookie/core';
import { AuthGuard } from './guards';
import { SendAlertComponent } from './send-alert/send-alert.component';
import { WaterSewageComponent } from './water-sewage/water-sewage.component';
import { StateTransportComponent } from './state-transport/state-transport.component';
import { IncidentListComponent } from './incident-list/incident-list.component';
import { PropertyTaxComponent } from './property-tax/property-tax.component';
import { BirthdeathComponent } from './birthdeath/birthdeath.component';
import { ParkingRealTimeComponent } from './parking-real-time/parking-real-time.component';
import { EnvironmentRealTimeComponent } from './environment-real-time/environment-real-time.component';
import { WasteRealTimeComponent } from './waste-real-time/waste-real-time.component';
import { RealWifiComponent } from './real-wifi/real-wifi.component';
import { RealPoleComponent } from './real-pole/real-pole.component';
import { RealTrafficComponent } from './real-traffic/real-traffic.component';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { MaterialModule } from './material.module';
import { RealFloodComponent } from './real-flood/real-flood.component';
import { RealSmartSurvilianceComponent } from './real-smart-surviliance/real-smart-surviliance.component';
import { SafePipe } from './safe.pipe';
import { UrbanSprawlComponent } from './urban-sprawl/urban-sprawl.component';
// import { RealFloodComponent } from './real-flood/real-flood.component'
declare var require: any;
export function highchartsFactory() {
  const hc = require('highcharts/highstock');
  const dd = require('highcharts/modules/exporting');
  dd(hc);
  return hc;
  }
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponentComponent,
    FooterComponentComponent,
    HomeComponentComponent,
    LoginComponentComponent,
    DashboardComponentComponent,
    IcccComponent,
    StateLevelComponent,
    DigitalAssetDashboardComponent,
    SmartParkingComponent,
    AirPurityComponent,
    WaterScadaComponent,
    StateAllComponent,
    IncidentDashboardComponent,
    EnvironmentComponent,
    ParkingComponent,
    SmartPoleComponent,
    SmartWaterComponent,
    SmartWifiComponent,
    SmartWasteComponent,
    SmartTransportComponent,
    SendAlertComponent,
    WaterSewageComponent,
    StateTransportComponent,
    IncidentListComponent,
    PropertyTaxComponent,
    BirthdeathComponent,
    ParkingRealTimeComponent,
    EnvironmentRealTimeComponent,
    WasteRealTimeComponent,
    RealWifiComponent,
    RealPoleComponent,
    RealTrafficComponent,
    RealFloodComponent,
    RealSmartSurvilianceComponent,
    SafePipe,
    UrbanSprawlComponent,
    // RealFloodComponent,
   
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ChartModule,
    HttpModule,
    HttpClientModule,
    NgxChartsModule,
    BrowserAnimationsModule,
    AmChartsModule,
    // MatButtonModule,
    // MatCheckboxModule,
    MaterialModule,
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyAYZQRDl6BO70jUXhVUAyqcCbcgaR0trAM'
    }),
  ],
  providers: [
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory,
      },
      CookieService,
    AuthGuard,  
  GetDigitalAssetType],
  bootstrap: [AppComponent]
})
export class AppModule { }
platformBrowserDynamic().bootstrapModule(AppModule)