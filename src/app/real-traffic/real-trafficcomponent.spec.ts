import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealTrafficComponent } from './real-traffic.component';

describe('RealTrafficComponent', () => {
  let component: RealTrafficComponent;
  let fixture: ComponentFixture<RealTrafficComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealTrafficComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTrafficComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
