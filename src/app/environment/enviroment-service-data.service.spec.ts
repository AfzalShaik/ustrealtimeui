import { TestBed, inject } from '@angular/core/testing';

import { EnviromentServiceDataService } from './enviroment-service-data.service';

describe('EnviromentServiceDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnviromentServiceDataService]
    });
  });

  it('should be created', inject([EnviromentServiceDataService], (service: EnviromentServiceDataService) => {
    expect(service).toBeTruthy();
  }));
});
