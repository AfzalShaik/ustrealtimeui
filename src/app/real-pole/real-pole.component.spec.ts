import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealPoleComponent } from './real-pole.component';

describe('RealPoleComponent', () => {
  let component: RealPoleComponent;
  let fixture: ComponentFixture<RealPoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealPoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealPoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
