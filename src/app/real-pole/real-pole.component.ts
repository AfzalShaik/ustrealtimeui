import { Component, OnInit, NgModule } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
// import { Time } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GetDigitalAssetType } from '../digital-asset-dashboard/digital-asset-dashboard.services';
import { toUnicode } from 'punycode';
declare var $: any;

@Component({
  selector: 'app-real-pole',
  templateUrl: './real-pole.component.html',
  styleUrls: ['./real-pole.component.css'],
  providers: [GraphsService, GetDigitalAssetType]
})
export class RealPoleComponent implements OnInit {
  zoom1: number = 17;
  lat: number = 13.0817545;
  lng: number = 80.2716236;
  paramData: any;
  city: any;
  info1: any;
  goodVar: boolean;
  manuallight: any;
  host: any;
  info: any;
  location: any;
  manual: any;
  light: any;
  main: boolean;

  getRealPoleAPI(x) {
    this.GS.getPoleAPI().subscribe(data => {
      // debugger;
      let obj = {};
      obj = data.data.Items[0];
      this.poleMarkers = [
        {
          lat: 13.081129688540713,
          lng: 80.27463475189359,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/light_on.png',
          info: '',
          type: '',
          manuallight: obj['payload'].M.manuallight.N,
          manual: obj['payload'].M.manual.N,
          location: obj['payload'].M.location.S,
          light: obj['payload'].M.light.N
        },
        {
          lat: 13.08196572142673,
          lng: 80.2718988986984,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/light_on.png',
          info: 'main',
          type: '',
          manuallight: obj['payload'].M.manuallight.N,
          manual: obj['payload'].M.manual.N,
          location: obj['payload'].M.location.S,
          light: obj['payload'].M.light.N
        }
      ]
    });
  }
  constructor(private GS: GraphsService, private route: ActivatedRoute, private serviceAssert: GetDigitalAssetType) { 
    // setInterval(() => { this.getRealPoleAPI(''); }, 1000 * 1 * 60);
  }

  ngOnInit() {
    this.getRealPoleAPI('');
    this.paramData = this.route.params.subscribe(params => {
      this.city = params.id;
      this.info1 = this.city;
    });
  }
  onLight() {
    let obj = {
      "SLCIds": "51277",
      "GatewayId": "4",
      "Dimming": "0"
    }
    this.GS.getRealLight(obj).subscribe(data => {
      console.log();
    })
  }
  offLight() {
    let obj = {
      "SLCIds": "51277",
      "GatewayId": "4",
      "Dimming": "100"
    }
    this.GS.getRealLight(obj).subscribe(data => {

    })
  }
  getPoleData() {
    this.GS.getPoleData().subscribe(data => {
      let obj = {};
      obj = data[0].Items[0];
      // console.log('objjjj ', obj);
      this.poleMarkers = [
        {
          lat:  13.037634720016372,
          lng:  80.21570706362218,
          label: '',
          draggable: false,
          iconUrl: '../assets/images/light_on.png',
          info: '',
          type: '',
          manuallight: obj['payload'].M.manuallight.N,
          manual: obj['payload'].M.manual.N,
          location: obj['payload'].M.location.S,
          light: obj['payload'].M.light.N
        }
      ]

    });
  }
  mapClicked($event: MouseEvent) {
    console.log('helloooooooooo', $event.coords);
    
  }
  clickedMarker(label: any,infoWindow, gm) {
    this.goodVar = true;
    this.manuallight = label.manuallight;
    this.manual = label.manual;
    this.light = label.light;
    this.location = label.location;
    if (label.info == 'main') {
      this.main = true;
    } else {
      this.main = false;
    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
   
  }
  mouseOver($event: MouseEvent, label: any, infoWindow, gm) {
    this.goodVar = true;
    this.manuallight = label.manuallight;
    this.manual = label.manual;
    this.light = label.light;
    this.location = label.location;
    if (label.info == 'main') {
      this.main = true;
    } else {
      this.main = false;
    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  poleMarkers: poleMarker[] = [
    {
      lat: 13.081129688540713,
      lng: 80.27463475189359,
      label: '',
      draggable: false,
      iconUrl: '',
      info: '',
      type: '',
      manuallight: '',
      manual: '',
      location: '',
      light: ''
    }
  ]
}
interface poleMarker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  info: any;
  type: string;
  manuallight: any;
  manual: any;
  location: any;
  light: any;
}