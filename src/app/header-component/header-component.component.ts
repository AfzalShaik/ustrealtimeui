import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { AuthGuard } from '../guards';
declare var $: any;
@Component({
  selector: 'app-header-component',
  templateUrl: './header-component.component.html',
  styleUrls: ['./header-component.component.css']
})
export class HeaderComponentComponent implements OnInit {

  logindata: any ={};
  constructor(private router: Router, private cookieService: CookieService, private authGuard:AuthGuard) {
    this.logindata = this.cookieService.getObject('loginResponse');
    // alert(this.logindata);
    $(document).ready(function () {
    $("#headActive").on('click', 'li', function () {
      $("#headActive li.active").removeClass("active");
      // adding classname 'active' to current click li 
      $(this).addClass("active");
    });
  });
   }

  ngOnInit() {
    // this.router.navigate(['weather/' + 'Coimbatore']);
    
  }
  // dashbord() {
  //   this.router.navigate(['dashboard']);
  // }

  logout() {
    this.authGuard.logout();
    this.router.navigate(['']);
    window.location.reload();
  }
}
