import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
declare var $: any;

@Component({
  selector: 'app-incident-list',
  templateUrl: './incident-list.component.html',
  styleUrls: ['./incident-list.component.css'],
  providers : [GraphsService]
})
export class IncidentListComponent implements OnInit {
  fire1: string;
  fire2: string;
  fire3: string;
  paramData: any;
  areaName: any;
  bombArray: any;
  transArray:any;
  floodArray:any;
  fiberArray:any;
  isafeArray:any;
  trafficArray:any;
	fireArray: any[];
  
  constructor(private GS: GraphsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.paramData = this.route.params.subscribe(params => {
      this.areaName = params.id;
    });
    this.fire1 = 'Moving Car Gutted In Fire In Coimbatore, No Casualties';
    this.fire2 = '2 Children Dead, One Missing In Annur Camp Fire';
    this.fire3 = 'Fires Break out in Kalveerampalayam Near Sowdambika gardens Building no 9';
    this.GS.getIncidents('Surveillance').subscribe(data => {
			if (data.length > 0) {
				this.bombArray = [];
				for (var i = 0; i < data.length; i++) {
					this.bombArray.push(data[i])
				}
			}
    });
    this.GS.getIncidents('Smart public events management').subscribe(data => {
			if (data.length > 0) {
				this.transArray = [];
				for (var i = 0; i < data.length; i++) {
					this.transArray.push(data[i])
				}
			}
    });
    this.GS.getIncidents('Flooding').subscribe(data => {
			if (data.length > 0) {
				this.floodArray = [];
				for (var i = 0; i < data.length; i++) {
					this.floodArray.push(data[i])
				}
			}
    });
    
    this.GS.getIncidents('Fiber').subscribe(data => {
			if (data.length > 0) {
				this.fiberArray = [];
				for (var i = 0; i < data.length; i++) {
					this.fiberArray.push(data[i])
				}
			}
    });
    
    this.GS.getIncidents('Panic').subscribe(data => {
			if (data.length > 0) {
				this.isafeArray = [];
				for (var i = 0; i < data.length; i++) {
					this.isafeArray.push(data[i])
				}
			}
    });
    
    this.GS.getIncidents('City Traffic').subscribe(data => {
			if (data.length > 0) {
				this.trafficArray = [];
				for (var i = 0; i < data.length; i++) {
					this.trafficArray.push(data[i])
				}
			}
		});
		// getFireIncidents() {
			this.GS.getIncidents('Fire').subscribe(data => {
				if (data.length > 0) {
					this.fireArray = [];
					for (var i = 0; i < data.length; i++) {
						// debugger;
						let desc = data[i].description;
						let descript = desc.split('5');
						let obj = data[i];
						obj.description = descript[0];
						this.fireArray.push(obj)
					}
				}
			});
		// }
  }

}
