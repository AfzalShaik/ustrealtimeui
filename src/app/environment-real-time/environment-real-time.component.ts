import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { GraphsService } from '../../services/graphs/graphs.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
declare var $: any;
 import { EnviromentServiceDataService} from './enviroment-service-data.service'

@Component({
  selector: 'app-environment-real-time',
  templateUrl: './environment-real-time.component.html',
  styleUrls: ['./environment-real-time.component.css'],
  providers: [GraphsService, EnviromentServiceDataService]
})
export class EnvironmentRealTimeComponent implements OnInit {

  status1: any;
	city: any;
	paramData: any;
	single2: { "name": string; "value": number; }[];
	label: any;
	timeVar: any;
	cityArray: any;
	deviceArray: any;
	info1: any;
	type: any;
	previousReading: any;
	currentReading: any;
	time: any;
	status: any;
	single: any[];
	single1: any[];
	view: any[] = [450, 220];
	// options
	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = true;
	xAxisLabel = '';
	showYAxisLabel = true;
	yAxisLabel = '';
	co2: any;
	so2: any;
	no2: any;
	o2: any;
	noise: any;
	options1: any;
	options2: any;
	options3: any;
	options4: any;
	options5: any;
	co2yAxis: any;
	cityMapData: any;
	zoom: number = 17;
	// iconUrl:'../assets/images/green.png';
	iconUrl: string = 'https://maps.google.com/mapfiles/kml/shapes/library_maps.png';
	// initial center position for the map 12.9612086,77.6102859,10z
	lat: number = 13.0817545;
  	lng: number = 80.2716236;
	co: any;
	light: any;
	o3: any;
	pm10: any;
	pm25: any;
	rh: any;
	temp: any;
	uva: any;
	uvb: any;
	envloginData: any;
	getRealParkingAPI(x) {
		this.GS.getParkingAPI().subscribe(dataEnv => {
			// debugger;
			let objj = {};
				// let len = dataEnv.length;
				// len = len-2;
				objj = dataEnv.data[0];
			
			this.envMarkers = [
				{
					lat: 13.0817545,
					lng: 80.2716236,
					label: '',
					draggable: false,
					iconUrl: './assets/images/reds.gif',
					info: '',
					type: '',
					parameters: objj['parameters']
				}
				]
		});
	  }
	constructor(private GS: GraphsService, private route: ActivatedRoute, private esd: EnviromentServiceDataService) {
		// setInterval(() => { this.getRealParkingAPI(''); }, 1000 * 1 * 60);
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
		$(document).ready(function () {
			//Toggle fullscreen
			$("#panel-fullscreen").click(function (e) {
				e.preventDefault();

				var $this = $(this);

				if ($this.children('i').hasClass('glyphicon-resize-full')) {
					$this.children('i').removeClass('glyphicon-resize-full');
					$this.children('i').addClass('glyphicon-resize-small');
					$('.holder').css({
						'min-height': '600px'
					});
					$('#tabsArea .panel-default').css({
						'padding': '20px',
						'margin-top': '95px'
					});
				}
				else if ($this.children('i').hasClass('glyphicon-resize-small')) {
					$this.children('i').removeClass('glyphicon-resize-small');
					$this.children('i').addClass('glyphicon-resize-full');
					$('.holder').css({
						'min-height': '250px'
					});
					$('#tabsArea .panel-default').css({
						'padding': '0px',
						'margin-top': '0px'
					});
				}
				$(this).closest('.panel').toggleClass('panel-fullscreen1');

			});
		})

		this.options = {
			title: { text: '' },
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			// series: [{
			// 		// data: [25.1, 25.2, 25.3, 25.4, 23.1, 23.7, 23.9, 29.9,29.4, 29.8, 71.5, 106.4, 129],
			// 		data: [46.47, 46.24, 46.08, 47.08, 47.33, 47.71, 47.68, 48.02, 48.92, 48.81, 49.20, 49.38, 49.78, 48.66, 48.41, 47.53, 46.67, 48.21, 48.77, 49.12, 49.03, 48.01, 48.47],
			// }]
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			series: [{
				name: 'CO2',
				data: [46.47, 46.24, 46.08, 47.08, 47.33, 47.71, 47.68, 48.02, 48.92, 48.81, 49.20, 49.38, 49.78, 48.66, 48.41, 47.53, 46.67, 48.21, 48.77, 49.12, 49.03, 48.01, 48.47],
				allowPointSelect: true
			}]
		};
		this.single = [
			{
				"name": "Working",
				"value": 16
			},
			{
				"name": "Stopped",
				"value": 2
			},
			{
				"name": "Maintanance",
				"value": 2
			},
			{
				"name": "Deployed",
				"value": 20
			}
		];
		this.single1 = [
			{
				"name": "Working",
				"value": 10
			},
			{
				"name": "Stopped",
				"value": 3
			},
			{
				"name": "Maintanance",
				"value": 2
			},
			{
				"name": "Deployed",
				"value": 15
			}
		];
		this.single2 = [
			{
				"name": "Working",
				"value": 26
			},
			{
				"name": "Stopped",
				"value": 5
			},
			{
				"name": "Maintanance",
				"value": 4
			},
			{
				"name": "Deployed",
				"value": 35
			}
		];
	}
	options: Object;
	onPointSelect(e) {
		// debugger;
		// alert(e.context.name)
		// this.serieName = e.context.name;
	}
	daily() {
		// alert(112)
		this.options = {
			title: { text: '' },
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].daily.CO2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			},
			]
		};
		this.options1 = {
			title: { text: '' },
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'SO2',
				data:this.cityMapData[0].daily.SO2 ,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			}]
		};
		this.options2 = {
			title: { text: '' },
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'NO2',
				data: this.cityMapData[0].daily.NO2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			}]
		};
		this.options3 = {
			title: { text: '' },
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'O2',
				data: this.cityMapData[0].daily.O2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			}]
		};
		this.options4 = {
			title: { text: '' },
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			yAxis: {
				title: { text: 'DB' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' DB';
                }
            },
			series: [{
				name: 'Noise',
				data: this.cityMapData[0].daily.Noise,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			}]
		};
		this.options5 = {
			title: { text: '' },
			xAxis: {
				categories: ['1-4 hrs','4-8 hrs','8-12 hrs','12-16 hrs','16-20 hrs','20-24 hrs' ],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].daily.CO2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			},{
				name: 'SO2',
				data: this.cityMapData[0].daily.SO2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			},{
				name: 'NO2',
				data: this.cityMapData[0].daily.NO2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			},{
				name: 'O2',
				data: this.cityMapData[0].daily.O2,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			},{
				name: 'Noise',
				data: this.cityMapData[0].daily.Noise,
				tooltip: {
					valueDecimals: 2
				},
				rangeSelector: {
					selected: 1
				},
			}]
		};
	}
	weekly() {
		this.options = {
			title: { text: '' },
			xAxis: {
				categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
				max:6
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].week.CO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options1 = {
			title: { text: '' },
			xAxis: {
				categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
				max:6
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'SO2',
				data: this.cityMapData[0].week.SO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options2 = {
			title: { text: '' },
			xAxis: {
				categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
				max:6
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'NO2',
				data: this.cityMapData[0].week.NO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options3= {
			title: { text: '' },
			xAxis: {
				categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
				max:6
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'O2',
				data: this.cityMapData[0].week.O2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options4= {
			title: { text: '' },
			xAxis: {
				categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
				max:6
			},
			yAxis: {
				title: { text: 'DB' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' DB';
                }
            },
			series: [{
				name: 'Noise',
				data: this.cityMapData[0].week.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options5= {
			title: { text: '' },
			xAxis: {
				categories: ['Mon','Tue','Wed','Thu','Fri','Sat','Sun' ],
				max:6
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].week.CO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'SO2',
				data: this.cityMapData[0].week.SO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'NO2',
				data: this.cityMapData[0].week.NO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'O2',
				data: this.cityMapData[0].week.O2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'Noise',
				data: this.cityMapData[0].week.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		
	}
	monthly() {
		this.options = {
			title: { text: '' },
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
				max:11
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].month.CO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options1 = {
			title: { text: '' },
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
				max:11
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'SO2',
				data: this.cityMapData[0].month.SO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options2 = {
			title: { text: '' },
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
				max:11
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'NO2',
				data: this.cityMapData[0].month.NO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options3 = {
			title: { text: '' },
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
				max:11
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'O2',
				data: this.cityMapData[0].month.O2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options4 = {
			title: { text: '' },
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
				max:11
			},
			yAxis: {
				title: { text: 'DB' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' DB';
                }
            },
			series: [{
				name: 'Noise',
				data: this.cityMapData[0].month.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options5 = {
			title: { text: '' },
			xAxis: {
				categories: ['Jan', 'Feb', 'Mar', 'Apr','may','jun','jul','aug','sep','oct','nov','dec'],
				max:11
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].month.CO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'SO2',
				data: this.cityMapData[0].month.SO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'NO2',
				data: this.cityMapData[0].month.NO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'O2',
				data: this.cityMapData[0].month.O2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'Noise',
				data: this.cityMapData[0].month.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};

	}
	quaterly() {
		this.options = {
			title: { text: '' },
			xAxis: {
				categories: ['2015(jan-june)','2015(july-dec)','2016(jan-june)','2016(july-dec)','2017(jan-june)','2017(july-dec)'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].quaterly.CO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options1 = {
			title: { text: '' },
			xAxis: {
				categories: ['2015(jan-june)','2015(july-dec)','2016(jan-june)','2016(july-dec)','2017(jan-june)','2017(july-dec)'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'SO2',
				data:this.cityMapData[0].quaterly.SO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options2 = {
			title: { text: '' },
			xAxis: {
				categories: ['2015(jan-june)','2015(july-dec)','2016(jan-june)','2016(july-dec)','2017(jan-june)','2017(july-dec)'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'NO2',
				data: this.cityMapData[0].quaterly.NO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options3 = {
			title: { text: '' },
			xAxis: {
				categories: ['2015(jan-june)','2015(july-dec)','2016(jan-june)','2016(july-dec)','2017(jan-june)','2017(july-dec)'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'O2',
				data: this.cityMapData[0].quaterly.O2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options4 = {
			title: { text: '' },
			xAxis: {
				categories: ['2015(jan-june)','2015(july-dec)','2016(jan-june)','2016(july-dec)','2017(jan-june)','2017(july-dec)'],
				max:5
			},
			yAxis: {
				title: { text: 'DB' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' DB';
                }
            },
			series: [{
				name: 'Noise',
				data: this.cityMapData[0].quaterly.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options5 = {
			title: { text: '' },
			xAxis: {
				categories: ['2015(jan-june)','2015(july-dec)','2016(jan-june)','2016(july-dec)','2017(jan-june)','2017(july-dec)'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].quaterly.CO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'SO2',
				data: this.cityMapData[0].quaterly.SO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'NO2',
				data: this.cityMapData[0].quaterly.NO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'O2',
				data: this.cityMapData[0].quaterly.O2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'Noise',
				data: this.cityMapData[0].quaterly.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
	}
	yearly() {
		this.options = {
			title: { text: '' },
			xAxis: {
				categories: ['2012', '2013', '2014', '2015','2016','2017'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].yearly.CO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options1 = {
			title: { text: '' },
			xAxis: {
				categories: ['2012', '2013', '2014', '2015','2016','2017'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'SO2',
				data: this.cityMapData[0].yearly.SO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options2 = {
			title: { text: '' },
			xAxis: {
				categories: ['2012', '2013', '2014', '2015','2016','2017'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'NO2',
				data: this.cityMapData[0].yearly.NO2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options3 = {
			title: { text: '' },
			xAxis: {
				categories: ['2012', '2013', '2014', '2015','2016','2017'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'O2',
				data: this.cityMapData[0].yearly.O2,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options4 = {
			title: { text: '' },
			xAxis: {
				categories: ['2012', '2013', '2014', '2015','2016','2017'],
				max:5
			},
			yAxis: {
				title: { text: 'DB' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' DB';
                }
            },
			series: [{
				name: 'Noise',
				data: this.cityMapData[0].yearly.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};
		this.options5 = {
			title: { text: '' },
			xAxis: {
				categories: ['2011', '2012', '2013', '2014', '2015','2016','2017'],
				max:5
			},
			yAxis: {
				title: { text: 'µg/m³' }
			  },
			tooltip:{
				borderWidth: 0,
				shadow: false,
				style: {
					padding: 0
				},
                formatter:function(){
                return this.y + ' µg/m³';
                }
            },
			series: [{
				name: 'CO2',
				data: this.cityMapData[0].yearly.CO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'SO2',
				data: this.cityMapData[0].yearly.SO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'NO2',
				data: this.cityMapData[0].yearly.NO2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'O2',
				data: this.cityMapData[0].yearly.O2,
				tooltip: {
					valueDecimals: 2
				}
			},{
				name: 'Noise',
				data: this.cityMapData[0].yearly.Noise,
				tooltip: {
					valueDecimals: 2
				}
			}]
		};

	}
	mapClicked($event: MouseEvent) {
		console.log('helloooooooooo', $event);
	  }
	ngOnInit() {
		this.getRealParkingAPI('');
		// let obj = {};
		// obj = {"userName" : "esfl_paqs",
		// "password":"bddf1f90#P"
		// 		}
		// this.GS.loginEnv(obj).subscribe(data => {
		// this.envloginData = data['_body'];
		// let access = JSON.parse(this.envloginData).token;
		
	// });
		this.timeVar = new Date();
		this.timeVar = this.timeVar;
		this.cityArray = ['Coimbatore', 'Madurai', 'Salem', 'Thanjavur', 'Tiruchirapalli', 'Vellore', 'Tirunelveli', 'Tiruppur', 'Thoothukudi', 'Erode'];
		this.GS.getDeviceInfo('Coimbatore').subscribe(data => {
			// console.log('device infoooooo ', JSON.stringify(data));
			this.deviceArray = data[0];
		});
		this.paramData = this.route.params.subscribe(params => {
			this.city = params.id;
			this.status1 = params.status;
			//  console.log('12345 ' +JSON.stringify(params));
			 this.getMapData(this.city);
			this.tabNavigation(this.status1);
			// this.GS.getRealEnvData().subscribe(dataEnv => {
				// console.log(JSON.stringify(dataEnv));
				// debugger;
			// 	let objj = {};
			// 	let len = dataEnv.length;
			// 	len = len-2;
			// 	objj = dataEnv[len];
			
			// this.envMarkers = [
			// 	{
			// 		lat: 12.839228,
			// 		lng: 77.662081,
			// 		label: '',
			// 		draggable: false,
			// 		iconUrl: './assets/images/reds.gif',
			// 		info: '',
			// 		type: '',
			// 		parameters: objj['parameters']

				// parameters: [
				// 	{
				// 		"name": "co",
				// 		"value": 1.2074981202950348
				// 	},
				// 	{
				// 		"name": "co2",
				// 		"value": 384.06756756756755
				// 	},
				// 	{
				// 		"name": "light",
				// 		"value": 9557
				// 	},
				// 	{
				// 		"name": "no2",
				// 		"value": 233.45623696816935
				// 	},
				// 	{
				// 		"name": "noise",
				// 		"value": 67.7957992553711
				// 	},
				// 	{
				// 		"name": "o3",
				// 		"value": 209.10056057491818
				// 	},
				// 	{
				// 		"name": "pm10",
				// 		"value": 336.6371765136719
				// 	},
				// 	{
				// 		"name": "pm25",
				// 		"value": 3.979114294052124
				// 	},
				// 	{
				// 		"name": "rh",
				// 		"value": 57.66880788029851
				// 	},
				// 	{
				// 		"name": "so2",
				// 		"value": 65.80182864214923
				// 	},
				// 	{
				// 		"name": "temp",
				// 		"value": 28.197372874698125
				// 	},
				// 	{
				// 		"name": "uva",
				// 		"value": 2.0142900943756104
				// 	},
				// 	{
				// 		"name": "uvb",
				// 		"value": 2.0142900943756104
				// 	}
				// ]
				// }
			// ]
			// this.pressureChanged(this.city);
		// });
		});

	}
	tabNavigation(status){
		if(status == 'pm'){

		}else if( status == 'co2'){
			// alert(1);
			$('#tabULL a[href="#tab1"]').trigger('click');
			this.daily();
		}else if( status == 'so'){
			// alert(2)
			$('#tabULL a[href="#tab2"]').trigger('click');
			this.daily();
			// this.getMapData(this.city);
		}else if( status == 'no'){
			// alert(2)
			$('#tabULL a[href="#tab3"]').trigger('click');
			this.daily();
			// this.getMapData(this.city);
		}else if( status == 'o'){
			// alert(2)
			$('#tabULL a[href="#tab4"]').trigger('click');
			this.daily();
		}else if( status == 'noise'){
			// alert(2)
			$('#tabULL a[href="#tab5"]').trigger('click');
			this.daily();
		}
	}
	onChartSelection(e) {
		alert(e.originalEvent.xAxis[0].min.toFixed(2))
		// this.from = e.originalEvent.xAxis[0].min.toFixed(2);
		// this.to = e.originalEvent.xAxis[0].max.toFixed(2);
	}

	clickedMarker(label: any, index: number) {
	// console.log(JSON.stringify(label));
	for(let i=0; i<label.parameters.length; i++) {
		if (label.parameters[i].name == 'co') {
			this.co = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'co2') {
			this.co2 = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'light') {
			this.light = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'no2') {
			this.no2 = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'noise') {
			this.noise = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'o3') {
			this.o3 = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'pm10') {
			this.pm10 = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'pm25') {
			this.pm25 = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'rh') {
			this.rh = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'so2') {
			this.so2 = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'temp') {
			this.temp = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'uva') {
			this.uva = label.parameters[i].value;
		}
		if (label.parameters[i].name == 'uvb') {
			this.uvb = label.parameters[i].value;
		}
	}
	}
	mouseOver($event: MouseEvent, label: any, infoWindow, gm) {
		console.log(JSON.stringify(label));
		for(let i=0; i<label.parameters.length; i++) {
			if (label.parameters[i].name == 'co') {
				this.co = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'co2') {
				this.co2 = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'light') {
				this.light = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'no2') {
				this.no2 = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'noise') {
				this.noise = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'o3') {
				this.o3 = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'pm10') {
				this.pm10 = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'pm25') {
				this.pm25 = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'rh') {
				this.rh = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'so2') {
				this.so2 = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'temp') {
				this.temp = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'uva') {
				this.uva = label.parameters[i].value;
			}
			if (label.parameters[i].name == 'uvb') {
				this.uvb = label.parameters[i].value;
			}
		}
		if (gm.lastOpen != null) {
		  gm.lastOpen.close();
		}
	
		gm.lastOpen = infoWindow;
	
		infoWindow.open();
	  }
	markerDragEnd(m: marker, $event: MouseEvent) {
		console.log('dragEnd', m, $event);
	}
	colorScheme = {
		domain: ['#5ca026', '#ed1c24', '#b87b06', '#AAAAAA']
	};

	getMapData(city){
		// alert("sssssssssssssss")
		city = city.toLowerCase();
		// alert(city);
		const data = 'EnvironmentGraphs?filter={"where":{"cityname":"'+city+'"}}';
		this.esd.getMapDataByCity(data).subscribe(data => {this.cityMapData = data;
		// console.log('12343234'+JSON.stringify(this.cityMapData));
		});
		
	}
	pressureChanged(x) {
		// alert('gggggg '+ x);
		if (x === 'Coimbatore') {
			this.lat = 11.0168;
			this.lng = 76.9558;
			this.label = 'C';
			this.zoom = 14;
			this.markers = [
				{
					lat: 11.014855,
					lng: 76.964711,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Aquatic Industries',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					time: this.timeVar,
					status: 'Working',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '32.22'
				},
				{
					lat: 11.0157749,
					lng: 76.9555643,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Kattupalayam',
					type: 'Water Pressure Sensor',
					previousReading: '29 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '23.22',
					so2: '43.11',
					no2: '31.3',
					o2: '44.22',
					noise: '43.22'
				},
				{
					lat: 11.011190,
					lng: 76.962854,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Tamil Nadu',
					type: 'Temperature Sensor',
					previousReading: '33 C',
					currentReading: '32 C',
					time: this.timeVar,
					status: 'Working',
					co2: '24.11',
					so2: '19.33',
					no2: '45.11',
					o2: '54.01',
					noise: '55.11'
				},
				{
					lat: 11.013846,
					lng: 76.952070,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Senjeri Ayyampalayam',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '11.22',
					so2: '26.34',
					no2: '37.11',
					o2: '29.11',
					noise: '41.23'
				},
				{
					lat: 11.016544,
					lng: 76.946208,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Raja Nagar',
					type: 'Water Pressure Sensor',
					previousReading: '30 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Working',
					co2: '39.11',
					so2: '22.11',
					no2: '24.11',
					o2: '27.11',
					noise: '29.11'
				},
				{
					lat: 11.012941,
					lng: 76.941101,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Tamil Nadu',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '39.11',
					so2: '31.11',
					no2: '41.11',
					o2: '51.11',
					noise: '43.11'
				},
				{
					lat: 11.023052,
					lng: 76.944806,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: '678582',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '33 C',
					time: this.timeVar,
					status: 'Working',
					co2: '44.44',
					so2: '22.32',
					no2: '54.22',
					o2: '49.11',
					noise: '12.22'
				},
				{
					lat: 11.019912,
					lng: 76.960122,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Tamil Nadu',
					type: 'Water Pressure Sensor',
					previousReading: '35 C',
					currentReading: '35 C',
					time: this.timeVar,
					status: 'Under Maintanace',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '32.22'
				},
				{
					lat: 11.022729,
					lng: 76.976007,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Bharathi Nagar ',
					type: 'Air Pressure Sensor',
					previousReading: '21 C',
					currentReading: '32 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '23.11',
					so2: '33.22',
					no2: '29.1',
					o2: '40.11',
					noise: '51.11'
				},
				{
					lat: 11.006420,
					lng: 76.968842,
					label: 'C',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Mathiyalagan Nagar',
					type: 'Air Pressure Sensor',
					previousReading: '28 C',
					currentReading: '25 C',
					time: this.timeVar,
					status: 'Working',
					co2: '34.11',
					so2: '41.22',
					no2: '31.11',
					o2: '32.11',
					noise: '39.11'
				},
				{
					lat: 11.000696,
					lng: 76.959726,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Mathiyalagan Nagar',
					type: 'Air Pressure Sensor',
					previousReading: '21 C',
					currentReading: '32 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '11.21',
					so2: '31.11',
					no2: '42.22',
					o2: '24.01',
					noise: '32.22'
				},
				{
					lat: 11.002405,
					lng: 76.956469,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Tamil Nadu',
					type: 'Water Pressure Sensor',
					previousReading: '35 C',
					currentReading: '35 C',
					time: this.timeVar,
					status: 'Under Maintanace',
					co2: '31.11',
					so2: '52.11',
					no2: '31.01',
					o2: '42.11',
					noise: '32.22'
				},
				{
					lat: 11.005812,
					lng: 76.943309,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Bharathi Nagar',
					type: 'Air Pressure Sensor',
					previousReading: '21 C',
					currentReading: '32 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '32.22'
				},
				{
					lat: 11.023812,
					lng: 78.961953,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Anaiyur',
					type: 'Water Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Working',
					co2: '43.22',
					so2: '32.01',
					no2: '27.01',
					o2: '34.01',
					noise: '25.22'
				},
				{
					lat: 11.023833,
					lng: 78.962254,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Anaiyur',
					type: 'Water Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				}
			];
			this.GS.getDeviceInfo('Coimbatore').subscribe(data => {
				// console.log('device infoooooo ', JSON.stringify(data));
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Madurai') {
			this.lat = 9.9252;
			this.lng = 78.1198;
			this.label = 'M';
			this.zoom = 14;
			this.markers = [
				{
					lat: 9.924518,
					lng: 78.123145,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: ' Yanaikkal Statue',
					type: 'Water Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.925270,
					lng: 78.119316,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Lochart Gap Point',
					type: 'Air Pressure Sensor',
					previousReading: '29 C',
					currentReading: '26 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.927003,
					lng: 78.131740,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Government Rajaji Hospital',
					type: 'Air Pressure Sensor',
					previousReading: '39 C',
					currentReading: '36 C',
					time: this.timeVar,
					status: 'Under Maintanace',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.926031,
					lng: 78.120990,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Annamalai Theatre',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '32 C',
					time: this.timeVar,
					status: 'Under Maintanace',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.926390,
					lng: 78.5023,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Madurai Sandhai',
					type: 'Water Pressure Sensor',
					previousReading: '31 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:9.932097,
					lng: 78.124037,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Sri Meenakshi Temple',
					type: 'Air Pressure Sensor',
					previousReading: '23 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.927468,
					lng: 78.114552,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'ISKCON Madurai',
					type: 'Water Pressure Sensor',
					previousReading: '28 C',
					currentReading: '30 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 9.934718,
					lng: 78.119359,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Sellur VAO Office',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 9.9079479,
		// 			lng: 78.1129516,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Asiya Apartments',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '29 C',
		// 			currentReading: '33 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 9.9057,
		// 			lng: 78.0918,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Palangantham',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '39 C',
		// 			currentReading: '33 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},

			];
			this.GS.getDeviceInfo('Madurai').subscribe(data => {
				// console.log('device infoooooo ', JSON.stringify(data));
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Salem') {
			this.lat = 11.6643;
			this.lng = 78.1460;
			this.label = 'S'
			this.zoom = 14;
			this.markers = [
				{
					lat: 11.662029,
					lng: 78.139207,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Sri Vinayagar Mariamman Temple',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.662828,
					lng: 78.148155,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'IDBI Bank K T Towers Omlur Main Road ',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
		
				},
				{
					lat: 11.667409,
					lng: 78.149786,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Nirmal Skywin Mall',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.668649,
					lng: 78.152833,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'BSNL Salem - GM Office',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.672095,
					lng: 78.145688,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'ARMC IVF Fertility Centre ',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.677223,
					lng: 78.142276,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Fairlands Police Station',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.676193,
					lng: 78.137577,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'SalemThyrocare Lab ',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.681888,
					lng:78.146331,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Michael Church',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
			];
			this.GS.getDeviceInfo('Salem').subscribe(data => {
				// console.log('device infoooooo ', JSON.stringify(data));
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Thanjavur') {
			this.lat = 10.7870;
			this.lng = 79.1378;
			this.zoom = 11;
			this.markers = [
				{
					lat: 10.809026304002867,
					lng: 79.1070556640625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Ramanathapuram Chief',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.695695224886434,
					lng: 79.1455078125,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Perumbur IInd Sethi',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.698394077836666,
					lng: 79.0191650390625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: '	Kurumpundi',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.683550089556267,
					lng: 79.11666870117188,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Kurungulam Keelpathi',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.676802582247122,
					lng: 79.22103881835938,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Alivoikkal',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.710538618820474,
					lng: 79.24850463867188,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Raghavambalpuram Part',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:10.73212771142289,
					lng: 79.21279907226562,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Manapparai-Pudukottai Rd',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 11.0407445,
		// 			lng: 79.06874249999998,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Paluvur',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 10.6568,
		// 			lng: 79.1012,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'North St',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 10.9789,
		// 			lng: 79.4057,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Pillayam Pettai',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
			];
			this.GS.getDeviceInfo('Thanjavur').subscribe(data => {
				// console.log('device infoooooo ', JSON.stringify(data));
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Tiruchirapalli') {
			this.lat = 10.7905;
			this.lng = 78.7047;
			this.zoom = 10;
			this.markers = [
				{
					lat: 10.760461055348594,
					lng: 78.7060546875,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Airport Officer Quarters, Wireless Rd, Anna Nagar, Tiruchirappalli',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.941191793456523,
					lng: 78.760986328125,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Irungalur',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.997816151968104,
					lng: 78.71429443359375,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Thathamangalam',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.755064433001635,
					lng: 78.651123046875,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Panjappur',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.032863880653736,
					lng: 78.717041015625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Musiri',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 10.701092906770022,
					lng: 78.80767822265625,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Elandapatti',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.00860051288406,
					lng: 78.48358154296875,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Valavandhi East',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:10.695695224886434,
					lng: 78.46435546875,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'K.Periapatti North',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 10.9229564,
		// 			lng: 78.74053570000001,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Samayapuram',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 10.763333,
		// 			lng: 78.46922,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Puthur',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},

			];
			this.GS.getDeviceInfo('Tiruchirapalli').subscribe(data => {
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Vellore') {
			this.lat = 12.9165;
			this.lng = 79.1325;
			this.zoom = 14;
			this.markers = [
				{
					lat: 12.916815,
					lng: 79.128385,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Periyar Park',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 12.920224,
					lng: 79.126797,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Tipu and Hyder Mahal',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 12.921186,
					lng: 79.140487,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Madeena masjid',
					type: 'Ayilam',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 12.923215,
					lng: 79.132719,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Union Bank of India ,28-D, Mundy Street, Near CMC',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 12.924616,
					lng: 79.124651,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Krishna Garden',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 12.926394,
					lng: 79.140100,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Hotel Saravana Bhavan',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 12.922357,
					lng: 79.141195,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Post Office Saidapet',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 12.9767298,
		// 			lng: 79.36485960000005,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Ammoor',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 12.8286068,
		// 			lng: 79.3066953,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Timiri',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 12.7655175,
		// 			lng: 78.9281896,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Muthukkumaran Malai Murgan Temple ( 70 years old )',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
			];
			this.GS.getDeviceInfo('Vellore').subscribe(data => {
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Tirunelveli') {
			this.lat = 8.7139;
			this.lng = 77.7567;
			this.zoom = 14;
			this.markers = [
				{
					lat: 8.711762	,
					lng: 77.750802,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Tirunelveli Medical College',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.711253,
					lng: 77.765822,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'J.A. Coconut Store',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat:8.718952,
					lng: 77.759299,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Fort English Church',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.715622,
					lng: 77.753677,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'GVS Green Mission Area',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.721518, 
					lng: 77.759643,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Sathakathullah Appa College Jummah Masjid',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.722409, 
					lng: 77.754986,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Bell Pins Ground',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.713501,
					lng: 77.763569,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'APS | APS Tirunalveli | Alleppey Parcel Service - Tirunelveli',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.719694, 
					lng: 77.764299,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Chella Suriya Hospital',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 8.8646356,
		// 			lng: 77.49600780000003,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Alangulam',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 8.6634393,
		// 			lng: 77.48202779999997,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Moolachi',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
			];
			this.GS.getDeviceInfo('Tirunelveli').subscribe(data => {
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Tiruppur') {
			this.lat = 11.1085;
			this.lng = 77.3411;
			this.zoom = 14;
			this.markers = [
				{
					lat: 11.109579, 
					lng: 77.336605,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Esi Hospital',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.107179, 
					lng: 77.339501,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Khaderpet Flea Market Branded X Port Surplus',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.111749, 
					lng: 77.346655,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Zaithoon Masjid',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.112527, 
					lng: 77.340789,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'TIRUPUR NORTH POST OFFICE',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.115791, 
					lng: 77.336154,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'AG Church',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.114675, 
					lng: 77.347591,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Roopa Nursing Home',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.118991, 
					lng: 77.344544,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Andipalayam Common Effluent Treatment Plant Pvt Ltd',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 11.121897, 
					lng: 77.341604,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'SE OFFICE /TNEB/TIRUPUR',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 10.7329354,
		// 			lng: 77.52181270000005,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Dharapuram',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 11.0053238,
		// 			lng: 77.56098429999997,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Kangayam',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
			];
			this.GS.getDeviceInfo('Tiruppur').subscribe(data => {
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		else if (x === 'Thoothukudi') {
			this.lat = 8.7642;
			this.lng = 78.1348;
			this.zoom = 14;
			this.markers = [
				{
					lat: 8.761602, 
					lng: 78.127213,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Krishna & Co.',
					type: 'Water Pressure Sensor',
					previousReading: '23 C',
					currentReading: '24 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.764168, 
					lng: 78.134830,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Elampuvanam VOC Rd',
					type: 'Air Pressure Sensor',
					previousReading: '25 C',
					currentReading: '19 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.759969, 
					lng: 78.130946,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Abiramee Timber Company',
					type: 'Air Pressure Sensor',
					previousReading: '27 C',
					currentReading: '22 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.766925, 
					lng: 78.135409,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Sethu Madhav Sea Shell Exports',
					type: 'Air Pressure Sensor',
					previousReading: '31 C',
					currentReading: '28 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.764677, 
					lng: 78.138607,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Godown Premises VOC Rd',
					type: 'Water Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Not Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.765483, 
					lng: 78.131483,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/green.png',
					info: 'Bharat Petroleum NH-45B',
					type: 'Air Pressure Sensor',
					previousReading: '33 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.763935, 
					lng: 78.137856,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/reds.gif',
					info: 'Industrial Mineral Co Tuticorin',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '27 C',
					time: this.timeVar,
					status: 'Working',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
				{
					lat: 8.760987, 
					lng: 78.146524,
					label: '',
					draggable: false,
					iconUrl: '../assets/images/yellow.png',
					info: 'Sri Santhana Mariamman Thirukoil',
					type: 'Water Pressure Sensor',
					previousReading: '32 C',
					currentReading: '29 C',
					time: this.timeVar,
					status: 'Under Maintanance',
					co2: '32.22',
					so2: '42.01',
					no2: '21.01',
					o2: '19.11',
					noise: '52.11'
				},
		// 		{
		// 			lat: 9.167084200000001,
		// 			lng: 78.44393200000002,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Sayalgudi',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 9.2191566,
		// 			lng: 78.71077309999998,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Ervadi',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
			];
			this.GS.getDeviceInfo('Thoothukudi').subscribe(data => {
				this.deviceArray = data[0];
				this.single = [
					{
						"name": "Working",
						"value": this.deviceArray.iotWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.iotStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.iotUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.iotDeployed
					}
				];
				this.single1 = [
					{
						"name": "Working",
						"value": this.deviceArray.wiredWorking
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.wiredStopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.wiredUnderMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.wiredDeployed
					}
				];
				this.single2 = [
					{
						"name": "Working",
						"value": this.deviceArray.working
					},
					{
						"name": "Stopped",
						"value": this.deviceArray.stopped
					},
					{
						"name": "Maintanance",
						"value": this.deviceArray.underMaintanance
					},
					{
						"name": "Deployed",
						"value": this.deviceArray.deployed
					}
				];
			});
		}
		// else if (x === 'Erode') {
		// 	this.lat = 11.3410;
		// 	this.lng = 77.7172;
		// 	this.markers = [
		// 		{
		// 			lat: 11.4469008,
		// 			lng: 77.68399610000006,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Bhavani',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '23 C',
		// 			currentReading: '24 C',
		// 			time: this.timeVar,
		// 			status: 'Working',
		// 			co2: '43.22'
		// 		},
		// 		{
		// 			lat: 11.4504099,
		// 			lng: 77.43003569999996,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Gobichettipalayam',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '25 C',
		// 			currentReading: '19 C',
		// 			time: this.timeVar,
		// 			status: 'Working',
		// 			co2: '43.22'
		// 		},
		// 		{
		// 			lat: 11.37825,
		// 			lng: 77.896927,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Tiruchengode',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '27 C',
		// 			currentReading: '22 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance',
		// 			co2: '43.22'
		// 		},
		// 		{
		// 			lat: 11.3131883,
		// 			lng: 77.78647769999998,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/red.png',
		// 			info: 'Kokkarayanpettai ',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '28 C',
		// 			time: this.timeVar,
		// 			status: 'Not Working'
		// 		},
		// 		{
		// 			lat: 11.1697857,
		// 			lng: 77.4514269,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/red.png',
		// 			info: 'Utukuli ',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '33 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Not Working'
		// 		},
		// 		{
		// 			lat: 11.2745621,
		// 			lng: 77.58260339999993,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Perundurai',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '33 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
		// 		{
		// 			lat: 11.0788297,
		// 			lng: 77.88674600000002,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Kodumudai',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
		// 		{
		// 			lat: 11.1627122,
		// 			lng: 77.59632710000005,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Chennimalai',
		// 			type: 'Water Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 11.2393791,
		// 			lng: 77.85316510000007,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/yellow.png',
		// 			info: 'Pasur',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '32 C',
		// 			currentReading: '29 C',
		// 			time: this.timeVar,
		// 			status: 'Under Maintanance'
		// 		},
		// 		{
		// 			lat: 11.1867508,
		// 			lng: 77.77381630000002,
		// 			label: '',
		// 			draggable: false,
		// 			iconUrl: '../assets/images/green.png',
		// 			info: 'Elumathur',
		// 			type: 'Air Pressure Sensor',
		// 			previousReading: '31 C',
		// 			currentReading: '27 C',
		// 			time: this.timeVar,
		// 			status: 'Working'
		// 		},
		// 	];
		// 	this.GS.getDeviceInfo('Erode').subscribe(data => {
		// 		this.deviceArray = data[0];
		// 		this.single = [
		// 			{
		// 				"name": "Working",
		// 				"value": this.deviceArray.iotWorking
		// 			},
		// 			{
		// 				"name": "Stopped",
		// 				"value": this.deviceArray.iotStopped
		// 			},
		// 			{
		// 				"name": "Maintanance",
		// 				"value": this.deviceArray.iotUnderMaintanance
		// 			},
		// 			{
		// 				"name": "Deployed",
		// 				"value": this.deviceArray.iotDeployed
		// 			}
		// 		];
		// 		this.single1 = [
		// 			{
		// 				"name": "Working",
		// 				"value": this.deviceArray.wiredWorking
		// 			},
		// 			{
		// 				"name": "Stopped",
		// 				"value": this.deviceArray.wiredStopped
		// 			},
		// 			{
		// 				"name": "Maintanance",
		// 				"value": this.deviceArray.wiredUnderMaintanance
		// 			},
		// 			{
		// 				"name": "Deployed",
		// 				"value": this.deviceArray.wiredDeployed
		// 			}
		// 		];
		// 		this.single2 = [
		// 			{
		// 				"name": "Working",
		// 				"value": this.deviceArray.working
		// 			},
		// 			{
		// 				"name": "Stopped",
		// 				"value": this.deviceArray.stopped
		// 			},
		// 			{
		// 				"name": "Maintanance",
		// 				"value": this.deviceArray.underMaintanance
		// 			},
		// 			{
		// 				"name": "Deployed",
		// 				"value": this.deviceArray.deployed
		// 			}
		// 		];
		// 	});
		// }
	}

	markers: marker[] = [
		{
			lat: 11.0168,
			lng: 76.9558,
			label: 'C',
			draggable: false,
			iconUrl: '',
			info: '',
			type: '',
			previousReading: '',
			currentReading: '',
			time: this.timeVar,
			status: '',
			co2: '',
			so2: '',
			no2: '',
			o2: '',
			noise: ''
		},

	];
	envMarkers: envMarker[] = [
		{
			lat: 13.0817545,
			lng: 80.2716236,
			label: 'C',
			draggable: false,
			iconUrl: '',
			info: '',
			type: '',
		parameters: []
		},
	]
}
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
	iconUrl: string;
	info: string;
	type: string;
	previousReading: string;
	currentReading: string;
	time: any;
	status: string;
	co2: string;
	so2: string;
	no2: string;
	o2: string;
	noise: string;
}
interface envMarker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
	iconUrl: string;
	info: string;
	type: string;
	parameters: any[];
}

$(window).scroll(function () {
	var scroll = $(window).scrollTop();

	//>=, not <=
	if (scroll >= 100) {
		//clearHeader, not clearheader - caps H
		$("#digitalDashboard").addClass("darkHeader");
	}
	if (scroll <= 10) {
		//clearHeader, not clearheader - caps H
		$("#digitalDashboard").removeClass("darkHeader");
	}
})