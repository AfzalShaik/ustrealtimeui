import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnvironmentRealTimeComponent } from './environment-real-time.component';

describe('EnvironmentRealTimeComponent', () => {
  let component: EnvironmentRealTimeComponent;
  let fixture: ComponentFixture<EnvironmentRealTimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnvironmentRealTimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnvironmentRealTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
