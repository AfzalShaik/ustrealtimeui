import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";

@Component({
  selector: 'app-birthdeath',
  templateUrl: './birthdeath.component.html',
  styleUrls: ['./birthdeath.component.css']
})
export class BirthdeathComponent implements OnInit {
  zoom: number = 8;
  lat:number = 11.0168;
  public chart: AmChart;
  lng:number = 76.9558;
  single: any[];
  single1:any[]
  multi:any[]
  view: any[] = [700, 300];
  view1: any[] = [600, 400];
  gradient = false;
  showLegend = false;
  showXAxis = true;
  showYAxis = true;
  option:string="Select Year";
  showXAxisLabel = true;
  // xAxisLabel = '';
  showYAxisLabel = true;
  // yAxisLabel = 'AGE WISE POPULATION';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  constructor(public AmCharts: AmChartsService) { }
  public whatever(){}

  ngOnInit() {
   this.multi =[
    {
        "name": "Coimbatore",
        "series": [
          {
            "name": "BIRTH",
            "value": 28000
          },
          {
            "name": "DEATH",
            "value": 22000
          }
        ]
      }, {
        "name": "Madurai",
        "series": [
          {
            "name": "BIRTH",
            "value": 26000
          },
          {
            "name": "DEATH",
            "value": 21000
          }
        ]
      }, {
        "name": "Salem",
        "series": [
          {
            "name": "BIRTH",
            "value": 22500
          },
          {
            "name": "DEATH",
            "value": 19500
          }
        ]
      }, {
        "name": "Thanjavur",
        "series": [
          {
            "name": "BIRTH",
            "value": 25300
          },
          {
            "name": "DEATH",
            "value": 22500
          }
        ]
      }, {
        "name": "Tiruchirapalli",
        "series": [
          {
            "name": "BIRTH",
            "value": 31500
          },
          {
            "name": "DEATH",
            "value": 28500
          }
        ]
      }, {
        "name": "Vellore",
        "series": [
          {
            "name": "BIRTH",
            "value": 28000
          },
          {
            "name": "DEATH",
            "value": 26709
          }
        ]
      }, {
        "name": "Tirunelveli",
        "series": [
          {
            "name": "BIRTH",
            "value": 24600
          },
          {
            "name": "DEATH",
            "value": 22000
          }
        ]
      }, {
        "name": "Tiruppur",
        "series": [
          {
            "name": "BIRTH",
            "value": 25700
          },
          {
            "name": "DEATH",
            "value": 22600
          }
        ]
      }, {
        "name": "Thoothukudi",
        "series": [
          {
            "name": "BIRTH",
            "value": 21500
          },
          {
            "name": "DEATH",
            "value": 19000
          }
        ]
      }, {
        "name": "Erode",
        "series": [
          {
            "name": "BIRTH",
            "value": 23890
          },
          {
            "name": "DEATH",
            "value": 23000
          }
        ]
      },
]

    this.single = [
      {
        "name": "0-10",
        "value": 6079056 
      },{
        "name": "11-20",
        "value":12431339
      },
      {
        "name": "21-30",
        "value": 13075289
      },
      {
        "name": "31-40",
        "value": 11652016
      },
      
      {
        "name": "41-50",
        "value": 9777265
      },
      {
        "name": "51-60",
        "value": 6804602
      },
      {
        "name": "61-70",
        "value": 4650978
      }
      , {
        "name": "70+",
        "value": 2116770
      } 
    ];
  }
  firstcheck(){
    alert("testy")
    }
  selectYear(a){
    if(a === "2015"){
        this.multi =[
            {
                "name": "Coimbatore",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 28000
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              }, {
                "name": "Madurai",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 26000
                  },
                  {
                    "name": "DEATH",
                    "value": 21000
                  }
                ]
              }, {
                "name": "Salem",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 22500
                  },
                  {
                    "name": "DEATH",
                    "value": 19500
                  }
                ]
              }, {
                "name": "Thanjavur",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 25300
                  },
                  {
                    "name": "DEATH",
                    "value": 22500
                  }
                ]
              }, {
                "name": "Tiruchirapalli",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 31500
                  },
                  {
                    "name": "DEATH",
                    "value": 28500
                  }
                ]
              }, {
                "name": "Vellore",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 28000
                  },
                  {
                    "name": "DEATH",
                    "value": 26709
                  }
                ]
              }, {
                "name": "Tirunelveli",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 24600
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              }, {
                "name": "Tiruppur",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 25700
                  },
                  {
                    "name": "DEATH",
                    "value": 22600
                  }
                ]
              }, {
                "name": "Thoothukudi",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 21500
                  },
                  {
                    "name": "DEATH",
                    "value": 19000
                  }
                ]
              }, {
                "name": "Erode",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 23890
                  },
                  {
                    "name": "DEATH",
                    "value": 23000
                  }
                ]
              },
        ]
      }
    if(a === "2016"){
        this.multi =[
            {
                "name": "Coimbatore",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 28000
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              }, {
                "name": "Madurai",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 26000
                  },
                  {
                    "name": "DEATH",
                    "value": 21000
                  }
                ]
              }, {
                "name": "Salem",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 22500
                  },
                  {
                    "name": "DEATH",
                    "value": 19500
                  }
                ]
              }, {
                "name": "Thanjavur",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 25300
                  },
                  {
                    "name": "DEATH",
                    "value": 22500
                  }
                ]
              }, {
                "name": "Tiruchirapalli",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 31500
                  },
                  {
                    "name": "DEATH",
                    "value": 28500
                  }
                ]
              }, {
                "name": "Vellore",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 28000
                  },
                  {
                    "name": "DEATH",
                    "value": 26709
                  }
                ]
              }, {
                "name": "Tirunelveli",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 24600
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              }, {
                "name": "Tiruppur",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 24600
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              }, {
                "name": "Thoothukudi",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 22600
                  },
                  {
                    "name": "DEATH",
                    "value": 19000
                  }
                ]
              }, {
                "name": "Erode",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 27000
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              },
        ]
    }
    if(a === "2017"){
        this.multi =[
            {
                "name": "Coimbatore",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 22340
                  },
                  {
                    "name": "DEATH",
                    "value": 18500
                  }
                ]
              }, {
                "name": "Madurai",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 26000
                  },
                  {
                    "name": "DEATH",
                    "value": 23000
                  }
                ]
              }, {
                "name": "Salem",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 23400
                  },
                  {
                    "name": "DEATH",
                    "value": 19500
                  }
                ]
              }, {
                "name": "Thanjavur",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 23500
                  },
                  {
                    "name": "DEATH",
                    "value": 22000
                  }
                ]
              }, {
                "name": "Tiruchirapalli",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 31500
                  },
                  {
                    "name": "DEATH",
                    "value": 28500
                  }
                ]
              }, {
                "name": "Vellore",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 28000
                  },
                  {
                    "name": "DEATH",
                    "value": 26709
                  }
                ]
              }, {
                "name": "Tirunelveli",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 24600
                  },
                  {
                    "name": "DEATH",
                    "value": 21500
                  }
                ]
              }, {
                "name": "Tiruppur",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 22700
                  },
                  {
                    "name": "DEATH",
                    "value": 19600
                  }
                ]
              }, {
                "name": "Thoothukudi",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 21300
                  },
                  {
                    "name": "DEATH",
                    "value": 19700
                  }
                ]
              }, {
                "name": "Erode",
                "series": [
                  {
                    "name": "BIRTH",
                    "value": 30000
                  },
                  {
                    "name": "DEATH",
                    "value": 26500
                  }
                ]
              },
        ]
      }
  }
  
  onSelect($event){
    // alert( JSON.stringify($event))
  }
  onSelect1(e){
    //   alert(JSON.stringify($event))
if(this.option === "2015"){
      if(e.name==="BIRTH" && e.series === "Coimbatore"){
      this.chart = this.AmCharts.makeChart( "chartdiv1", {
        "name":"Coimbatore",
         "type": "pie",
         "hideCredits":true,
         "theme": "light",
         "dataProvider": [{
           "country": "Male",
           "value": 13500
         }, 
         {
           "country": "Female",
           "value": 14500
         },
        ],
        //  "titles": [{
        //    "text": "Coimbatore"
        //  }],
         "valueField": "value",
         "titleField": "country",
         "outlineAlpha": 0.4,
         "depth3D": 15,
         "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
         "angle": 30,
         "export": {
           "enabled": true
         }
       });
  } else if(e.name==="BIRTH" && e.series === "Madurai"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12500
       }, 
       {
         "country": "Female",
         "value": 13500
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Salem"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12500
       }, 
       {
         "country": "Female",
         "value": 10500
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Thanjavur"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12300
       }, 
       {
         "country": "Female",
         "value": 13000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Tiruchirapalli"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 16500
       }, 
       {
         "country": "Female",
         "value": 15000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Vellore"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 14500
       }, 
       {
         "country": "Female",
         "value": 13500
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //    "balloonText": "Birth in Vellore [[title]] ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Tirunelveli"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12600
       }, 
       {
         "country": "Female",
         "value": 12000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //    "balloonText": "Birth in Tirunelveli [[title]] ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Tiruppur"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 13700
       }, 
       {
         "country": "Female",
         "value": 12000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //    "balloonText": "Birth in Tiruppur [[title]] ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Thoothukudi"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12500
       }, 
       {
         "country": "Female",
         "value": 9000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Erode"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12890
       }, 
       {
         "country": "Female",
         "value": 11000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
}
}if(this.option === "2016"){
    if(e.name==="BIRTH" && e.series === "Coimbatore"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 13500
       }, 
       {
         "country": "Female",
         "value": 14500
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12500
     }, 
     {
       "country": "Female",
       "value": 13500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12500
     }, 
     {
       "country": "Female",
       "value": 10500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15, "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Salem [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12300
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 16500
     }, 
     {
       "country": "Female",
       "value": 15000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Tiruchirapalli [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 14500
     }, 
     {
       "country": "Female",
       "value": 13500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Vellore [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12600
     }, 
     {
       "country": "Female",
       "value": 12000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Tirunelveli [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13600
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Tiruppur [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12600
     }, 
     {
       "country": "Female",
       "value": 10000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Thoothukudi [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 14500
     }, 
     {
       "country": "Female",
       "value": 12500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
    //  "balloonText": "Birth in Erode [[title]] ([[percents]]%) : [[value]] ",
    "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}if(this.option === "2017"){
    if(e.name==="BIRTH" && e.series === "Coimbatore"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12340
       }, 
       {
         "country": "Female",
         "value": 10000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //    "balloonText": "Birth in Coimbatore [[title]] ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if(e.name==="BIRTH" && e.series === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 14000
     }, 
     {
       "country": "Female",
       "value": 12000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Madurai [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12400
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Salem [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12500
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Thanjavur [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 20500
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
    //  "balloonText": "Birth in Tiruchirapalli [[title]] ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 14500
     }, 
     {
       "country": "Female",
       "value": 13500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12600
     }, 
     {
       "country": "Female",
       "value": 12000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10300
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12600
     }, 
     {
       "country": "Female",
       "value": 10000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.name==="BIRTH" && e.series === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 15500
     }, 
     {
       "country": "Female",
       "value": 14500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Birth ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}if(this.option === "2017" && e.name==="DEATH" ){
    if( e.series === "Coimbatore"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 9500
       }, 
       {
         "country": "Female",
         "value": 9000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if( e.series === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12000
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10000
     }, 
     {
       "country": "Female",
       "value": 9500
     },
    
    ],
    "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12500
     }, 
     {
       "country": "Female",
       "value": 9500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 14500
     }, 
     {
       "country": "Female",
       "value": 14000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13700
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 11500
     }, 
     {
       "country": "Female",
       "value": 10000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10600
     }, 
     {
       "country": "Female",
       "value": 9000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10700
     }, 
     {
       "country": "Female",
       "value": 9000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13500
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}else if(this.option === "2016" && e.name==="DEATH" ){
    if( e.series === "Coimbatore"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12000
       }, 
       {
         "country": "Female",
         "value": 10000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if( e.series === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12000
     }, 
     {
       "country": "Female",
       "value": 11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 11000
     }, 
     {
       "country": "Female",
       "value": 8500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13300
     }, 
     {
       "country": "Female",
       "value": 12000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 15500
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13700
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12000
     }, 
     {
       "country": "Female",
       "value": 10000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10600
     }, 
     {
       "country": "Female",
       "value": 9000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10700
     }, 
     {
       "country": "Female",
       "value": 8300
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13000
     }, 
     {
       "country": "Female",
       "value": 9000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}else if(this.option === "2015" && e.name==="DEATH" ){
    if( e.series === "Coimbatore"){
    this.chart = this.AmCharts.makeChart( "chartdiv1", {
      "name":"Coimbatore",
       "type": "pie",
       "hideCredits":true,
       "theme": "light",
       "dataProvider": [{
         "country": "Male",
         "value": 12000
       }, 
       {
         "country": "Female",
         "value": 10000
       },
      ],
      //  "titles": [{
      //    "text": "Coimbatore"
      //  }],
       "valueField": "value",
       "titleField": "country",
       "outlineAlpha": 0.4,
       "depth3D": 15,
       "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
       // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
       "angle": 30,
       "export": {
         "enabled": true
       }
     });
} else if( e.series === "Madurai"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10500
     }, 
     {
       "country": "Female",
       "value": 10500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Salem"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 11000
     }, 
     {
       "country": "Female",
       "value": 8000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Thanjavur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12500
     }, 
     {
       "country": "Female",
       "value": 12000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if( e.series === "Tiruchirapalli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 15500
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Vellore"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 13700
     }, 
     {
       "country": "Female",
       "value": 13000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Tirunelveli"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12000
     }, 
     {
       "country": "Female",
       "value": 10000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Tiruppur"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10600
     }, 
     {
       "country": "Female",
       "value": 10000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Thoothukudi"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 10500
     }, 
     {
       "country": "Female",
       "value": 8500
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
} else if(e.series === "Erode"){
  this.chart = this.AmCharts.makeChart( "chartdiv1", {
    "name":"Coimbatore",
     "type": "pie",
     "hideCredits":true,
     "theme": "light",
     "dataProvider": [{
       "country": "Male",
       "value": 12000
     }, 
     {
       "country": "Female",
       "value":11000
     },
    ],
    //  "titles": [{
    //    "text": "Coimbatore"
    //  }],
     "valueField": "value",
     "titleField": "country",
     "outlineAlpha": 0.4,
     "depth3D": 15,
     "balloonText": "[[title]] Death ([[percents]]%) : [[value]] ",
     // "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
     "angle": 30,
     "export": {
       "enabled": true
     }
   });
}
}
  }
  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }
  
  mapClicked($event: MouseEvent) {
   
  }
  
  markerDragEnd( $event: MouseEvent) {
    console.log('dragEnd', $event);
  }
  
  markers = [
	  {
		  lat: 11.0168,
      lng: 76.9558,
      label: 'A',
      city: 'Coimbatore',
		  draggable: false
	  },
	  {
      lat: 11.6643,
      lng: 78.1460,
      label: 'B',
      city: 'Salem',
		  draggable: false
	  },
	  {
		  lat: 9.9252,
      lng: 78.1198,
      label: 'C',
      city: 'Madurai',
		  draggable: false
	  }
  ]
 
}
