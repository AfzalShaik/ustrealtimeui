import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BirthdeathComponent } from './birthdeath.component';

describe('BirthdeathComponent', () => {
  let component: BirthdeathComponent;
  let fixture: ComponentFixture<BirthdeathComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BirthdeathComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BirthdeathComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
