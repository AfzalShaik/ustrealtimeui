import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartWaterComponent } from './smart-water.component';

describe('SmartWaterComponent', () => {
  let component: SmartWaterComponent;
  let fixture: ComponentFixture<SmartWaterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartWaterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartWaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
