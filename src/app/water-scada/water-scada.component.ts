import { Component, OnInit } from '@angular/core';
import { MouseEvent } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GraphsService } from '../../services/graphs/graphs.service';
import { GetDigitalAssetType } from './../digital-asset-dashboard/digital-asset-dashboard.services';
import { BrowserModule } from '@angular/platform-browser';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ChartModule } from 'angular2-highcharts';
// import { AmChartsService, AmChart } from "@amcharts/amcharts3-angular";
declare var $: any;

@Component({
  selector: 'app-water-scada',
  templateUrl: './water-scada.component.html',
  styleUrls: ['./water-scada.component.css']
})
export class WaterScadaComponent implements OnInit {
  options: any;
  severity1: boolean;
  deficientVar: boolean;
  excessVar: boolean;
  moderateVar: boolean;
  goodVar: boolean;
  severeVar: boolean;
  noiseVar: boolean;
  oVar: boolean;
  noVar: boolean;
  soVar: boolean;
  coVar: boolean;
  severity: any;
  type: any;
  city: any;
  area: any;
  selectedCityName: any;
  smartCityInfo: any;
  todayDate: any;
  zoom: any = 8;
  zoom1: any = 8;
  // initial center position for the map
	lat: number = 11.3272889;
  lng: number = 78.07734;
  lat1: number = 11.3272889;
  lng1: number = 78.07734;
  latParking: number = 11.3272889;
  lngParking: number = 78.07734;
  zoomControl: boolean = true;
  constructor(private serviceAssert:GetDigitalAssetType, private router: Router) { 
    $(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
  }
  view: any[] = [750, 400];
  colorScheme4 = {
    domain: [ '#b87b06','#5ca026']
  };

  data111 = [
    {
      "name": "Coimbatore",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1422
        },
        {
          "name": "Capacity",
          "value": 1600
        },
      ]
    },
    {
      "name": "Madurai",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1322
        },
        {
          "name": "Capacity",
          "value": 1500
        },
      ]
    },
    {
      "name": "Salem",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 559
        },
        {
          "name": "Capacity",
          "value": 238
        },
      ]
    },
    {
      "name": "Thanjavur",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 395
        },
        {
          "name": "Capacity",
          "value": 398
        },
      ]
    },
    {
      "name": "Tiruchirapalli",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 842
        },
        {
          "name": "Capacity",
          "value": 1023
        },
      ]
    },
    {
      "name": "Vellore",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 544
        },
        {
          "name": "Capacity",
          "value": 655
        },
      ]
    },
    {
      "name": "Tirunelveli",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1477
        },
        {
          "name": "Capacity",
          "value": 1497
        },
      ]
    },
    {
      "name": "Tiruppur",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 791
        },
        {
          "name": "Capacity",
          "value": 855
        },
      ]
    },
    {
      "name": "Thoothukudi",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1359
        },
        {
          "name": "Capacity",
          "value": 1477
        },
      ]
    },
    {
      "name": "Erode",
      "series": [
        {
          "name": "Distribution Volume",
          "value": 1477
        },
        {
          "name": "Capacity",
          "value": 925
        },
      ]
    },
  ]
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';

  showDataLabel=true;

  ngOnInit() {
    this.todayDate = new Date();
  }
  clickedMarker1(label: any, index: number) {
    console.log(`clicked the marker: ${JSON.stringify(label) || index}`);
    this.area = label.label;
    this.city = label.city;
    this.type = label.type;
    this.severity = label.severity;
    
  }
  mapClicked1($event: MouseEvent) {
    console.log('helloooooooooo', $event);
  }
  mouseOver1($event: MouseEvent, label: any, city: any, severity: any, type: any, infoWindow, gm) {
    // debugger;
    this.area = label;
    this.city = city;
    this.type = type;
    this.severity = severity;
    if (this.type == 'Excess Suply') {
      this.excessVar = true;
      this.deficientVar = false;
    } else if (this.type == 'Deficient') {
      this.deficientVar = true;
      this.excessVar = false;
    }
    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
  markers1: marker1[] = [

    {
      lat: 11.12254185885016,
      lng: 76.92789541992192,
      label: '1422KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Coimbatore',
      severity: '1600KL',
      type: 'Excess Suply',
    },
    {
      lat: 9.929476032244802,
      lng: 78.10891906330528,
       label: '1322KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Madurai',
      severity: '1500KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.659632671616086,
      lng: 78.14169532816027,
       label: '559KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Salem',
      severity: '238KL',
      type: 'Deficient',
    },
    {
      lat: 10.787,
      lng: 79.1378,
      label: '395KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Thanjavur',
      severity: '398KL',
      type: 'Excess Suply',
    },
    {
      lat: 10.7905,
      lng: 78.7047,
      label: '842KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Tiruchirapalli',
      severity: '1023KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.1085,
      lng: 77.3411,
      label: '791KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Tiruppur',
      severity: '855KL',
      type: 'Excess Suply',
    },
    {
      lat: 12.9165,
      lng: 79.1325,
      label: '544KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Vellore',
      severity: '655KL',
      type: 'Excess Suply',
    },
    {
      lat: 8.7139,
      lng: 77.7567,
      label: '1477KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Tirunelveli',
      severity: '1497KL',
      type: 'Excess Suply',
    },
    {
      lat: 8.7642,
      lng: 78.1348,
      label: '1359KL',
      draggable: true,
      iconUrl: '../assets/images/blue.png',
      city: 'Thoothkudi',
      severity: '1477KL',
      type: 'Excess Suply',
    },
    {
      lat: 11.3410,
      lng: 77.7172,
      label: '1477KL',
      draggable: true,
      iconUrl: '../assets/images/purple.png',
      city: 'Erode',
      severity: '925KL',
      type: 'Deficient',
    },
  ]
}
interface marker1 {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
  iconUrl: string;
  city: string;
  severity: any;
  type: any;
}