import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import {CookieService} from 'angular2-cookie/core';
import { HttpClient } from '@angular/common/http';
import {AppURL} from '../apiUrl';
@Injectable()
export class AuthGuard implements CanActivate {
 
    constructor(private router: Router, private cookieService: CookieService, private httpClient: HttpClient) { }
 
    // canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //     if (localStorage.getItem('currentUser')) {
    //         // logged in so return true
    //         return true;
    //     }
 
    //     // not logged in so redirect to login page with the return url
    //     this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    //     return false;
    // }
    postLogin(loginData) {
        return this.httpClient.post(AppURL.api + `citizens/login`, loginData)
              .map((res: Response) => res);
      }
    isAuthenticated(): boolean {
        // return tokenNotExpired('token');
        if (this.cookieService.getObject('token')) {
          return true;
        }else {
          return false;
        }
      }
    canActivate() {
        
        if (this.isAuthenticated()) {
          return true;
        } else {
          this.router.navigate(['']);
        }
      }
      finishAuthentication(token, loginResponce): void {
        this.cookieService.putObject('token', token);
        // localStorage.setItem('token', token);
        if (loginResponce.id) {
          this.router.navigate(['dashboard']);
          window.location.reload();
        
        }else {
          this.router.navigate(['']);
          this.logout();
        }
      }

      logout(): void {
        // localStorage.removeItem('token');
        this.cookieService.remove('token');
        this.cookieService.removeAll();
      }
}